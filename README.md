## Little Limited Star Wars

# Development notes
This project is meant to present software development skills and was prepared with the greatest knowledge I have.
I tried to develop that project with docker and it succeeded somehow, however, I encountered some problems with CORS and model binding which does not exist in the non-dockerized environment.
There are obviously some things which can (or even should) be improved and I will talk about them with pleasure.
The application is not finished regarding business requirements. There are still places to improve existing codebase too. The front-end part looks poor since I'm not a regular front-end developer (I'm involved rather in the backend in daily activities).
Testing is done for presentation purposes. The goal was to show what my approach for unit/integration tests is. Unfortunately, I spent the most time on the docker part.
There is a definitely better way for awaiting database creation, I tried the approach with bash script inside docker but it didn't work for me.

To run the application you have to fulfill some preconditions:
* API
    1. You have to have SQL Server installed. Remember the connection string/port/local address it is working on and modify launchSettings.json under LittleLimitedSWAPI\LittleLimitedSWAPI\Properties directory. DB_CONNECTION_STRING variable is to be modified there.
    2. You have to have node.js installed, version 13
    3. You have to have angular CLI installed, the newest version
    4. Open C# solution with Visual Studio 2019 (It can be maybe opened with VS 2017 too, however, I didn't check that)
    5. Modify appsettings.Development.json and set two flags there (ShouldSeedDatabase, ShouldWaitForDBCreation) to true.
    6. Run ctrl+f5 for 'without debugging' mode or f5 for 'with debugging' mode.
    7. When migrations were performed and the database was seeded, the application will listen on localhost:5000 address and your default browser will open with swagger.
    8. You can investigate available endpoints in swagger

* UI:
    1. Go into LLSWUI directory, open console and run 'npm install' command.
    2. After all packages are installed run 'ng serve' command.
    3. Your default browser should open with the localhost:4200 address. UI, as mentioned before is poor and not finished. Codebase in angular can be easily enhanced.
    4. For now, only dashboard and people tabs are working and providing some data for a user. 

# Notes:
Dockerized UI should work with host IP (in my case this is IP address of host Linux machine from docker toolbox) address written in http.get method in person.service. I've read that https://forums.docker.com/t/connection-refused-when-you-try-to-connect-to-a-service-port-started-on-host/11508/10
so on production, it should be handled differently.
You can start SQL Server in docker (go to docker-compose\docker-compose-db), but in this case, you have to have docker installed and you have to know the address (port is mapped in docker-compose) of docker host in order to prepare proper connection string.
Not every model reflects perfect DDD approach, but it presents the idea itself 