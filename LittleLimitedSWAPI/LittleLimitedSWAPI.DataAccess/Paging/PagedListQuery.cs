﻿using LittleLimitedSWAPI.Domain;
using LittleLimitedSWAPI.Domain.Paging;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace LittleLimitedSWAPI.DataAccess.Paging
{
    public class PagedListQuery<TPageableEntity> : IPagedListQuery<TPageableEntity>
        where TPageableEntity : class, IPageableEntity
    {
        public async Task<PagedResult<TPageableEntity>> GetPagedResult(IQueryable<TPageableEntity> query, Page page)
        {
            int totalCount = query.Count();
            query = query.OrderBy(x => x.Id);

            int skippedItems = (page.Number - 1) * page.Size;
            var items = await query
                 .Skip(skippedItems)
                 .Take(page.Size)
                 .ToListAsync();

            return new PagedResult<TPageableEntity>
            {
                TotalCount = totalCount,
                Items = items
            };
        }
    }
}