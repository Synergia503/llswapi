﻿using LittleLimitedSWAPI.DataAccess.EF;
using LittleLimitedSWAPI.Domain;
using LittleLimitedSWAPI.Domain.Paging;
using LittleLimitedSWAPI.Domain.People;
using LittleLimitedSWAPI.Domain.People.Queries;
using LittleLimitedSWAPI.Domain.People.ReadModels;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace LittleLimitedSWAPI.DataAccess.Query.People
{
    public class GetPagedPeopleQueryHandler : IQueryHandler<GetPeople, PagedResult<PersonReadModel>>
    {
        private readonly LLContext _llContext;
        private readonly IPagedListQuery<Person> _getPeopleQuery;

        public GetPagedPeopleQueryHandler(
            LLContext llContext,
            IPagedListQuery<Person> getPeopleQuery)
        {
            _llContext = llContext;
            _getPeopleQuery = getPeopleQuery;
        }

        public async Task<PagedResult<PersonReadModel>> HandleQuery(GetPeople query)
        {
            Page page = query.MapPageableQueryToPage();
            IQueryable<Person> peopleQuery = _llContext
                .Persons
                .Include(p => p.PersonToPersonAttributes)
                .ThenInclude(p => p.PersonAttribute)
                .AsQueryable();

            PagedResult<Person> pagedPeople = await _getPeopleQuery.GetPagedResult(peopleQuery, page);
            return MapToPagedPersonReadModel(pagedPeople);
        }

        private PagedResult<PersonReadModel> MapToPagedPersonReadModel(PagedResult<Person> pagedPeople)
        {
            return new PagedResult<PersonReadModel>
            {
                Items = pagedPeople
                .Items
                .Select(p =>
                    new PersonReadModel
                    {
                        PersonId = p.Id,
                        PersonName = p.Name
                    })
                .ToList(),
                TotalCount = pagedPeople.TotalCount
            };
        }
    }
}