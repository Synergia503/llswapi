﻿using LittleLimitedSWAPI.DataAccess.EF;
using LittleLimitedSWAPI.Domain;
using LittleLimitedSWAPI.Domain.People;
using LittleLimitedSWAPI.Domain.People.Queries;
using LittleLimitedSWAPI.Domain.People.ReadModels;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace LittleLimitedSWAPI.DataAccess.Query.People
{
    public class CalculatePersonWinnerHandler : IQueryHandler<CalculatePersonWinner, PersonWinnerReadModel>
    {
        private readonly LLContext _llContext;

        public CalculatePersonWinnerHandler(LLContext llContext)
        {
            _llContext = llContext;
        }

        public async Task<PersonWinnerReadModel> HandleQuery(CalculatePersonWinner query)
        {
            Person attacker = await _llContext
                .Persons
                .Include(p => p.PersonToPersonAttributes)
                   .ThenInclude(p => p.PersonAttribute)
                .SingleOrDefaultAsync(p => p.Id == query.AttackerPersonId);

            Person defender = await _llContext
                .Persons
                .Include(p => p.PersonToPersonAttributes)
                   .ThenInclude(p => p.PersonAttribute)
                .SingleOrDefaultAsync(p => p.Id == query.DefenderPersonId);

            PersonToPersonAttribute attackerAttributeValue = attacker
                  .PersonToPersonAttributes
                  .SingleOrDefault(x => x.PersonAttribute.Id == query.CrucialPersonAttributeId);

            PersonToPersonAttribute defenderAttributeValue = defender
             .PersonToPersonAttributes
             .SingleOrDefault(x => x.PersonAttribute.Id == query.CrucialPersonAttributeId);

            PersonToPersonAttribute winner = CalculateWinner(attackerAttributeValue, defenderAttributeValue);

            return Map(winner.Person);
        }

        private PersonToPersonAttribute CalculateWinner(PersonToPersonAttribute attackerAttributeValue, PersonToPersonAttribute defenderAttributeValue)
        {
            return attackerAttributeValue > defenderAttributeValue;
        }

        private PersonWinnerReadModel Map(Person person)
        {
            return new PersonWinnerReadModel
            {
                PersonId = person.Id,
                Name = person.Name
            };
        }
    }
}