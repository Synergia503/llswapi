﻿using LittleLimitedSWAPI.DataAccess.EF;
using LittleLimitedSWAPI.Domain;
using LittleLimitedSWAPI.Domain.People;
using LittleLimitedSWAPI.Domain.People.Queries;
using LittleLimitedSWAPI.Domain.People.ReadModels;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace LittleLimitedSWAPI.DataAccess.Query.People
{
    public class GetPersonByIdQueryHandler : IQueryHandler<GetPersonById, PersonWithAttributesReadModel>
    {
        private readonly LLContext _llContext;

        public GetPersonByIdQueryHandler(LLContext llContext)
        {
            _llContext = llContext;
        }

        public async Task<PersonWithAttributesReadModel> HandleQuery(GetPersonById query)
        {
            Person person = await _llContext
                .Persons
                .Include(p => p.PersonToPersonAttributes)
                .ThenInclude(p => p.PersonAttribute)
                .SingleOrDefaultAsync(p => p.Id == query.PersonId);

            return Map(person);
        }

        private PersonWithAttributesReadModel Map(Person person)
        {
            return new PersonWithAttributesReadModel
            {
                PersonId = person.Id,
                PersonName = person.Name,
                PersonAttributes = person
                .PersonToPersonAttributes
                .Select(ptpa =>
                            new PersonToPersonAttributeReadModel
                            {
                                PersonAttributeId = ptpa.PersonAttributeId,
                                Name = ptpa.PersonAttribute.Name,
                                Value = ptpa.Value
                            })
                .ToList()
            };
        }
    }
}