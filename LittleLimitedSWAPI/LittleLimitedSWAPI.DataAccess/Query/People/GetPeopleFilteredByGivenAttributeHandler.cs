﻿using LittleLimitedSWAPI.DataAccess.EF;
using LittleLimitedSWAPI.Domain;
using LittleLimitedSWAPI.Domain.Paging;
using LittleLimitedSWAPI.Domain.People;
using LittleLimitedSWAPI.Domain.People.Queries;
using LittleLimitedSWAPI.Domain.People.ReadModels;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace LittleLimitedSWAPI.DataAccess.Query.People
{
    public class GetPeopleFilteredByGivenAttributeHandler
        : IQueryHandler<GetPeopleFilteredByGivenAttribute, PagedResult<PeopleFilteredByAttributeReadModel>>
    {
        private readonly LLContext _llContext;
        private readonly IPagedListQuery<Person> _getPeopleQuery;

        public GetPeopleFilteredByGivenAttributeHandler(
            LLContext llContext,
            IPagedListQuery<Person> getPeopleQuery)
        {
            _llContext = llContext;
            _getPeopleQuery = getPeopleQuery;
        }

        public async Task<PagedResult<PeopleFilteredByAttributeReadModel>> HandleQuery(GetPeopleFilteredByGivenAttribute query)
        {
            Page page = query.MapPageableQueryToPage();
            IQueryable<Person> filteredPeople = _llContext
                    .Persons
                    .Include(p => p.PersonToPersonAttributes)
                    .Where(p => p.PersonToPersonAttributes.Any(ptpa => ptpa.PersonAttributeId == query.CommonAttributeId));

            PagedResult<Person> filteredPagedPeople = await _getPeopleQuery.GetPagedResult(filteredPeople, page);
            return MapFilteredToPagedPersonReadModel(filteredPagedPeople, query.CommonAttributeId);
        }

        private PagedResult<PeopleFilteredByAttributeReadModel> MapFilteredToPagedPersonReadModel(PagedResult<Person> filteredPagedPeople, int commonAttributeId)
        {
            return new PagedResult<PeopleFilteredByAttributeReadModel>
            {
                Items = filteredPagedPeople
                .Items
                .Select(p =>
                    new PeopleFilteredByAttributeReadModel
                    {
                        PersonId = p.Id,
                        PersonName = p.Name,
                        PersonAttributeValue = p.PersonToPersonAttributes.SingleOrDefault(ptpa => ptpa.PersonAttributeId == commonAttributeId).Value,
                    })
                .ToList(),
                TotalCount = filteredPagedPeople.TotalCount
            };
        }
    }
}