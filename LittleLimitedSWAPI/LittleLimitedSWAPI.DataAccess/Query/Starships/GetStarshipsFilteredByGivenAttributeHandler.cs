﻿using LittleLimitedSWAPI.DataAccess.EF;
using LittleLimitedSWAPI.Domain;
using LittleLimitedSWAPI.Domain.Paging;
using LittleLimitedSWAPI.Domain.Starships;
using LittleLimitedSWAPI.Domain.Starships.Queries;
using LittleLimitedSWAPI.Domain.Starships.ReadModels;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace LittleLimitedSWAPI.DataAccess.Query.Starships
{
    public class GetStarshipsFilteredByGivenAttributeHandler
        : IQueryHandler<GetStarshipsFilteredByGivenAttribute, PagedResult<StarshipsFilteredByAttributeReadModel>>
    {
        private readonly LLContext _llContext;
        private readonly IPagedListQuery<Starship> _getStarshipsQuery;

        public GetStarshipsFilteredByGivenAttributeHandler(
            LLContext llContext,
            IPagedListQuery<Starship> getStarshipsQuery)
        {
            _llContext = llContext;
            _getStarshipsQuery = getStarshipsQuery;
        }

        public async Task<PagedResult<StarshipsFilteredByAttributeReadModel>> HandleQuery(GetStarshipsFilteredByGivenAttribute query)
        {
            Page page = query.MapPageableQueryToPage();
            IQueryable<Starship> filteredStarships = _llContext
                    .Starships
                    .Include(s => s.StarshipToStarshipAttributes)
                    .Where(s => s.StarshipToStarshipAttributes.Any(stsa => stsa.StarshipAttributeId == query.CommonAttributeId));

            PagedResult<Starship> filteredPagedStarships = await _getStarshipsQuery.GetPagedResult(filteredStarships, page);
            return MapFilteredToPagedStarshipReadModel(filteredPagedStarships, query.CommonAttributeId);
        }

        private PagedResult<StarshipsFilteredByAttributeReadModel> MapFilteredToPagedStarshipReadModel(PagedResult<Starship> filteredPagedStarships, int commonAttributeId)
        {
            return new PagedResult<StarshipsFilteredByAttributeReadModel>
            {
                Items = filteredPagedStarships
                .Items
                .Select(s =>
                    new StarshipsFilteredByAttributeReadModel
                    {
                        StarshipId = s.Id,
                        StarshipName = s.Name,
                        StarshipAttributeValue = s.StarshipToStarshipAttributes.SingleOrDefault(stsa => stsa.StarshipAttributeId == commonAttributeId).Value,
                    })
                .ToList(),
                TotalCount = filteredPagedStarships.TotalCount
            };
        }
    }
}