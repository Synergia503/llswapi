﻿using LittleLimitedSWAPI.DataAccess.EF;
using LittleLimitedSWAPI.Domain;
using LittleLimitedSWAPI.Domain.Starships;
using LittleLimitedSWAPI.Domain.Starships.Queries;
using LittleLimitedSWAPI.Domain.Starships.ReadModels;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace LittleLimitedSWAPI.DataAccess.Query.Starships
{
    public class CalculateStarshipWinnerHandler : IQueryHandler<CalculateStarshipWinner, StarshipWinnerReadModel>
    {
        private readonly LLContext _llContext;

        public CalculateStarshipWinnerHandler(LLContext llContext)
        {
            _llContext = llContext;
        }

        public async Task<StarshipWinnerReadModel> HandleQuery(CalculateStarshipWinner query)
        {
            Starship attacker = await _llContext
                .Starships
                .Include(s => s.StarshipToStarshipAttributes)
                   .ThenInclude(s => s.StarshipAttribute)
                .SingleOrDefaultAsync(s => s.Id == query.AttackerStarshipId);

            Starship defender = await _llContext
                .Starships
                .Include(s => s.StarshipToStarshipAttributes)
                   .ThenInclude(s => s.StarshipAttribute)
                .SingleOrDefaultAsync(s => s.Id == query.DefenderStarshipId);

            StarshipToStarshipAttribute attackerAttributeValue = attacker
                  .StarshipToStarshipAttributes
                  .SingleOrDefault(x => x.StarshipAttribute.Id == query.CrucialStarshipAttributeId);

            StarshipToStarshipAttribute defenderAttributeValue = defender
             .StarshipToStarshipAttributes
             .SingleOrDefault(x => x.StarshipAttribute.Id == query.CrucialStarshipAttributeId);

            StarshipToStarshipAttribute winner = CalculateWinner(attackerAttributeValue, defenderAttributeValue);

            return Map(winner.Starship);
        }

        private StarshipToStarshipAttribute CalculateWinner(StarshipToStarshipAttribute attackerAttributeValue, StarshipToStarshipAttribute defenderAttributeValue)
        {
            return attackerAttributeValue > defenderAttributeValue;
        }

        private StarshipWinnerReadModel Map(Starship starship)
        {
            return new StarshipWinnerReadModel
            {
                StarshipId = starship.Id,
                Name = starship.Name
            };
        }
    }
}