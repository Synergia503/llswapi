﻿using LittleLimitedSWAPI.DataAccess.EF;
using LittleLimitedSWAPI.Domain;
using LittleLimitedSWAPI.Domain.Paging;
using LittleLimitedSWAPI.Domain.Starships;
using LittleLimitedSWAPI.Domain.Starships.Queries;
using LittleLimitedSWAPI.Domain.Starships.ReadModels;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace LittleLimitedSWAPI.DataAccess.Query.Starships
{
    public class GetPagedStarshipsQueryHandler : IQueryHandler<GetStarships, PagedResult<StarshipReadModel>>
    {
        private readonly LLContext _llContext;
        private readonly IPagedListQuery<Starship> _getStarshipsQuery;

        public GetPagedStarshipsQueryHandler(
            LLContext llContext,
            IPagedListQuery<Starship> getStarshipsQuery)
        {
            _llContext = llContext;
            _getStarshipsQuery = getStarshipsQuery;
        }

        public async Task<PagedResult<StarshipReadModel>> HandleQuery(GetStarships query)
        {
            Page page = query.MapPageableQueryToPage();
            IQueryable<Starship> starshipsQuery = _llContext
                .Starships
                .Include(s => s.StarshipToStarshipAttributes)
                .ThenInclude(s => s.StarshipAttribute);

            PagedResult<Starship> pagedStarships = await _getStarshipsQuery.GetPagedResult(starshipsQuery, page);
            return MapToPagedStarshipReadModel(pagedStarships);
        }

        private PagedResult<StarshipReadModel> MapToPagedStarshipReadModel(PagedResult<Starship> pagedStarships)
        {
            return new PagedResult<StarshipReadModel>
            {
                Items = pagedStarships
                .Items
                .Select(s =>
                    new StarshipReadModel
                    {
                        StarshipId = s.Id,
                        StarshipName = s.Name
                    })
                .ToList(),
                TotalCount = pagedStarships.TotalCount
            };
        }
    }
}