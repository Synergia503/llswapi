﻿using LittleLimitedSWAPI.DataAccess.EF;
using LittleLimitedSWAPI.Domain;
using LittleLimitedSWAPI.Domain.Starships;
using LittleLimitedSWAPI.Domain.Starships.Queries;
using LittleLimitedSWAPI.Domain.Starships.ReadModels;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace LittleLimitedSWAPI.DataAccess.Query.Starships
{
    public class GetStarshipByIdQueryHandler : IQueryHandler<GetStarshipById, StarshipWithAttributesReadModel>
    {
        private readonly LLContext _llContext;

        public GetStarshipByIdQueryHandler(LLContext llContext)
        {
            _llContext = llContext;
        }

        public async Task<StarshipWithAttributesReadModel> HandleQuery(GetStarshipById query)
        {
            Starship starship = await _llContext
                .Starships
                .Include(s => s.StarshipToStarshipAttributes)
                .ThenInclude(s => s.StarshipAttribute)
                .SingleOrDefaultAsync(s => s.Id == query.StarshipId);

            return Map(starship);
        }

        private StarshipWithAttributesReadModel Map(Starship starship)
        {
            return new StarshipWithAttributesReadModel
            {
                StarshipId = starship.Id,
                StarshipName = starship.Name,
                StarshipAttributes = starship
                .StarshipToStarshipAttributes
                .Select(stsa =>
                            new StarshipToStarshipAttributeReadModel
                            {
                                StarshipAttributeId = stsa.StarshipAttributeId,
                                Name = stsa.StarshipAttribute.Name,
                                Value = stsa.Value
                            })
                .ToList()
            };
        }
    }
}