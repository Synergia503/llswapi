﻿using LittleLimitedSWAPI.DataAccess.EF.EntityConfigurationMaps.People;
using LittleLimitedSWAPI.DataAccess.EF.EntityConfigurationMaps.Starships;
using LittleLimitedSWAPI.Domain.People;
using LittleLimitedSWAPI.Domain.Starships;
using Microsoft.EntityFrameworkCore;

namespace LittleLimitedSWAPI.DataAccess.EF
{
    public class LLContext : DbContext
    {
        public LLContext(DbContextOptions<LLContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new PersonMap());
            builder.ApplyConfiguration(new PersonAttributeMap());
            builder.ApplyConfiguration(new PersonToPersonAttributeMap());
            builder.ApplyConfiguration(new StarshipMap());
            builder.ApplyConfiguration(new StarshipAttributeMap());
            builder.ApplyConfiguration(new StarshipToStarshipAttributeMap());
            builder.ApplyConfiguration(new StarshipKindMap());
        }

        public virtual DbSet<Person> Persons { get; set; }
        public virtual DbSet<PersonAttribute> PersonAttributes { get; set; }
        public virtual DbSet<Starship> Starships { get; set; }
        public virtual DbSet<StarshipAttribute> StarshipAttributes { get; set; }
        public virtual DbSet<StarshipKind> StarshipKinds { get; set; }
    }
}