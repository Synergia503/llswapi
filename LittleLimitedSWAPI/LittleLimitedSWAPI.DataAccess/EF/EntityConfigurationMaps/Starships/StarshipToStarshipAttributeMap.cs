﻿using LittleLimitedSWAPI.Domain.Starships;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LittleLimitedSWAPI.DataAccess.EF.EntityConfigurationMaps.Starships
{
    public class StarshipToStarshipAttributeMap : IEntityTypeConfiguration<StarshipToStarshipAttribute>
    {
        public void Configure(EntityTypeBuilder<StarshipToStarshipAttribute> builder)
        {
            builder
                .HasKey(b => new { b.StarshipId, b.StarshipAttributeId });

            builder
                .Property(b => b.Value)
                .IsRequired();

            builder
                .HasOne(b => b.Starship)
                .WithMany(b => b.StarshipToStarshipAttributes)
                .HasForeignKey(b => b.StarshipId);

            builder
                .HasOne(b => b.StarshipAttribute)
                .WithMany(b => b.StarshipToStarshipAttributes)
                .HasForeignKey(b => b.StarshipAttributeId);
        }
    }
}