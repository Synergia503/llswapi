﻿using LittleLimitedSWAPI.Domain.Starships;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LittleLimitedSWAPI.DataAccess.EF.EntityConfigurationMaps.Starships
{
    public class StarshipKindMap : IEntityTypeConfiguration<StarshipKind>
    {
        public void Configure(EntityTypeBuilder<StarshipKind> builder)
        {
            builder
                .HasKey(b => b.Id);

            builder
              .Property(b => b.Id)
              .IsRequired();

            builder
                .Property(b => b.StarshipKindName)
                .HasMaxLength(32)
                .IsRequired();

            builder
               .HasMany(b=>b.Starships)
               .WithOne(b => b.StarshipKind)
               .HasForeignKey(b => b.StarshipKindId)
               .IsRequired();
        }
    }
}