﻿using LittleLimitedSWAPI.Domain.Starships;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LittleLimitedSWAPI.DataAccess.EF.EntityConfigurationMaps.Starships
{
    public class StarshipMap : IEntityTypeConfiguration<Starship>
    {
        public void Configure(EntityTypeBuilder<Starship> builder)
        {
            builder
                .HasKey(b => b.Id);

            builder
                .Property(b => b.Id)
                .IsRequired();

            builder
                .Property(b => b.Name)
                .HasMaxLength(32)
                .IsRequired();

            builder
                .HasMany(b => b.People)
                .WithOne(b => b.Starship)
                .HasForeignKey(b => b.StarshipId);
        }
    }
}