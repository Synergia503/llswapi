﻿using LittleLimitedSWAPI.Domain.Starships;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LittleLimitedSWAPI.DataAccess.EF.EntityConfigurationMaps.Starships
{
    public class StarshipAttributeMap : IEntityTypeConfiguration<StarshipAttribute>
    {
        public void Configure(EntityTypeBuilder<StarshipAttribute> builder)
        {
            builder
                .HasKey(b => b.Id);

            builder
                .Property(b => b.Id)
                .IsRequired();

            builder
                .Property(b => b.Name)
                .HasMaxLength(32)
                .IsRequired();
        }
    }
}