﻿using LittleLimitedSWAPI.Domain.People;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LittleLimitedSWAPI.DataAccess.EF.EntityConfigurationMaps.People
{

    public class PersonToPersonAttributeMap : IEntityTypeConfiguration<PersonToPersonAttribute>
    {
        public void Configure(EntityTypeBuilder<PersonToPersonAttribute> builder)
        {
            builder
                .HasKey(b => new { b.PersonId, b.PersonAttributeId });

            builder
                .Property(b => b.Value)
                .IsRequired();

            builder
                .HasOne(b => b.Person)
                .WithMany(b => b.PersonToPersonAttributes)
                .HasForeignKey(b => b.PersonId);

            builder
                .HasOne(b => b.PersonAttribute)
                .WithMany(b => b.PersonToPersonAttributes)
                .HasForeignKey(b => b.PersonAttributeId);
        }
    }
}