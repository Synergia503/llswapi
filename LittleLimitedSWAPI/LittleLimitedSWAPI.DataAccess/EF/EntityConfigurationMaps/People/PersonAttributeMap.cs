﻿using LittleLimitedSWAPI.Domain.People;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LittleLimitedSWAPI.DataAccess.EF.EntityConfigurationMaps.People
{
    public class PersonAttributeMap : IEntityTypeConfiguration<PersonAttribute>
    {
        public void Configure(EntityTypeBuilder<PersonAttribute> builder)
        {
            builder
                .HasKey(b => b.Id);

            builder
                .Property(b => b.Id)
                .IsRequired();

            builder
                .Property(b => b.Name)
                .HasMaxLength(32)
                .IsRequired();
        }
    }
}