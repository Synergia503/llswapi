﻿namespace LittleLimitedSWAPI.DataAccess.Extensions
{
    public static class ArrayExtensions
    {
        public static bool IsNullOrEmpty<T>(this T[] collection)
        {
            if (collection == null || collection.Length == 0)
                return true;

            return false;
        }
    }
}