﻿using LittleLimitedSWAPI.DataAccess.EF;
using Microsoft.EntityFrameworkCore;

namespace LittleLimitedSWAPI.DataAccess.Initialize
{
    public class DataInitializer : IDataInitializer
    {
        private readonly LLContext _llContext;
        private readonly string _insertDummyDataScriptName = "InsertDummyData.sql";

        public DataInitializer(LLContext llContext)
        {
            _llContext = llContext;
        }

        public void InitializeDatabase()
        {
            string sqlScriptContent = DummySqlScript
                .ForScriptName(_insertDummyDataScriptName)
                .FindSqlScriptInRuntimeDirectory()
                .GetScriptContent();

            _llContext.Database.ExecuteSqlCommand(sqlScriptContent);
            _llContext.SaveChanges();
        }
    }

    public interface IDataInitializer
    {
        void InitializeDatabase();
    }
}