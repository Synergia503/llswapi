﻿using LittleLimitedSWAPI.DataAccess.Extensions;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace LittleLimitedSWAPI.DataAccess.Initialize
{
    public class DummySqlScript
    {
        private readonly string _dummySqlScriptName;
        private string _insertDummyDataSqlScriptPath;

        private DummySqlScript(string dummySqlScriptName)
        {
            _dummySqlScriptName = dummySqlScriptName;
        }

        public static DummySqlScript ForScriptName(string dummySqlScriptName)
            => new DummySqlScript(dummySqlScriptName);

        public DummySqlScript FindSqlScriptInRuntimeDirectory()
        {
            string locationToSearchInsertDummyDataScript = FindRunTimeLocation();

            _insertDummyDataSqlScriptPath = GetFileInGivenLocation(locationToSearchInsertDummyDataScript)
                .FirstOrDefault();

            return this;
        }

        public string GetScriptContent()
         => File.ReadAllText(_insertDummyDataSqlScriptPath);

        private string FindRunTimeLocation()
        {
            string runtimeParentLocation = Directory.GetParent(Assembly.GetEntryAssembly().Location)?.FullName;

            if (string.IsNullOrEmpty(runtimeParentLocation))
            {
                throw new Exception("No assembly location.");
            }

            return runtimeParentLocation;
        }

        private string[] GetFileInGivenLocation(string runtimeLocation)
        {
            string[] fileNames = Directory.GetFiles(runtimeLocation, _dummySqlScriptName, SearchOption.AllDirectories);

            if (fileNames.IsNullOrEmpty())
            {
                throw new Exception("There are no file under runtime location.");
            }

            return fileNames;
        }
    }
}