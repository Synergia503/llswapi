﻿USE [LLSWAPI]

SET IDENTITY_INSERT [dbo].[PersonAttributes] ON 

INSERT [dbo].[PersonAttributes] ([Id], [Name]) VALUES (1, N'Stamina')

INSERT [dbo].[PersonAttributes] ([Id], [Name]) VALUES (2, N'Morale')

INSERT [dbo].[PersonAttributes] ([Id], [Name]) VALUES (3, N'Strength')

INSERT [dbo].[PersonAttributes] ([Id], [Name]) VALUES (4, N'Intelligence')

INSERT [dbo].[PersonAttributes] ([Id], [Name]) VALUES (5, N'Speed')

INSERT [dbo].[PersonAttributes] ([Id], [Name]) VALUES (6, N'Cleverness')

INSERT [dbo].[PersonAttributes] ([Id], [Name]) VALUES (7, N'Art')

SET IDENTITY_INSERT [dbo].[PersonAttributes] OFF

SET IDENTITY_INSERT [dbo].[StarshipKinds] ON 

INSERT [dbo].[StarshipKinds] ([Id], [StarshipKindName]) VALUES (1, N'Cruiser')

INSERT [dbo].[StarshipKinds] ([Id], [StarshipKindName]) VALUES (2, N'Star of Life')

INSERT [dbo].[StarshipKinds] ([Id], [StarshipKindName]) VALUES (3, N'Death Star')

INSERT [dbo].[StarshipKinds] ([Id], [StarshipKindName]) VALUES (4, N'Bomber')

INSERT [dbo].[StarshipKinds] ([Id], [StarshipKindName]) VALUES (5, N'Destroyer')

INSERT [dbo].[StarshipKinds] ([Id], [StarshipKindName]) VALUES (6, N'Warship')

INSERT [dbo].[StarshipKinds] ([Id], [StarshipKindName]) VALUES (7, N'Battleship')

SET IDENTITY_INSERT [dbo].[StarshipKinds] OFF

SET IDENTITY_INSERT [dbo].[Starships] ON 

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (1, N'Clubman', 4)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (2, N'Ridgeline', 3)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (3, N'A6', 6)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (4, N'Arnage', 7)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (5, N'riolet', 7)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (6, N'7 Series', 2)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (7, N'Concorde', 4)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (8, N'E-Series', 1)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (9, N'S40', 7)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (10, N'F250', 4)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (11, N'E-Series', 2)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (12, N'Jetta', 3)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (13, N'A6', 1)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (14, N'Ram Wan B150', 7)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (15, N'MX-5', 6)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (16, N'190E', 6)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (17, N'Maxima', 4)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (18, N'Matrix', 1)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (19, N'Integra', 4)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (20, N'Jetta', 3)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (21, N'Malibu', 6)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (22, N'Eclipse', 2)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (23, N'Outback', 7)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (24, N'Sportage', 6)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (25, N'Ram Van B350', 7)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (26, N'Eclipse', 3)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (27, N'Rendezvous', 7)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (28, N'Alero', 5)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (29, N'Ram 1500', 7)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (30, N'V8 Vantage S', 2)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (31, N'Summit', 1)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (32, N'Silverado 1500', 5)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (33, N'911', 2)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (34, N'V70', 1)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (35, N'Sierra 3500', 1)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (36, N'240', 1)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (37, N'S70', 5)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (38, N'Thunderbird', 1)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (39, N'Accord', 7)

INSERT [dbo].[Starships] ([Id], [Name], [StarshipKindId]) VALUES (40, N'Aztek', 4)

SET IDENTITY_INSERT [dbo].[Starships] OFF

SET IDENTITY_INSERT [dbo].[Persons] ON 

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (1, N'Gwenaëllena', 19)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (2, N'Marie-thérèse', 24)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (3, N'Intéressant', 27)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (4, N'Estée', 7)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (5, N'Märta', 26)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (6, N'Laurélie', 25)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (7, N'Maïwenn', 36)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (8, N'Médiamass', 20)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (9, N'Sòng', 21)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (10, N'Maïlis', 28)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (11, N'Thérèsa', 37)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (12, N'Ruì', 24)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (13, N'Léane', 27)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (14, N'Dà', 7)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (15, N'Táng', 12)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (16, N'Andréanne', 25)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (17, N'Zhì', 22)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (18, N'Léone', 32)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (19, N'Cécile', 3)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (20, N'Loïc', 22)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (21, N'Hélène', 4)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (22, N'Laurène', 3)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (23, N'Mén', 15)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (24, N'Clémence', 6)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (25, N'Wá', 7)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (26, N'Cloé', 38)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (27, N'Maïwenn', 40)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (28, N'Görel', 17)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (29, N'Märta', 3)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (30, N'Aimée', 27)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (31, N'Andréa', 36)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (32, N'Mélia', 6)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (33, N'Pélagie', 12)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (34, N'Lucrèce', 17)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (35, N'Marie-hélène', 16)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (36, N'Angélique', 28)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (37, N'Ráo', 39)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (38, N'Mélys', 13)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (39, N'Angélique', 36)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (40, N'Léonore', 30)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (41, N'Cinéma', 37)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (42, N'Frédérique', 17)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (43, N'Gaëlle', 26)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (44, N'Magdalène', 1)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (45, N'Audréanne', 30)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (46, N'Simplifiés', 8)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (47, N'Stéphanie', 30)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (48, N'Mégane', 19)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (49, N'Danièle', 2)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (50, N'Bérénice', 36)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (51, N'Marie-noël', 1)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (52, N'Léana', 34)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (53, N'Maëlla', 37)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (54, N'Irène', 4)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (55, N'Alizée', 11)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (56, N'Aimée', 8)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (57, N'Léandre', 39)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (58, N'Mégane', 35)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (59, N'Liè', 15)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (60, N'Maïlis', 35)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (61, N'Marie-ève', 19)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (62, N'Bérengère', 1)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (63, N'Méryl', 13)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (64, N'Mélanie', 6)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (65, N'Naéva', 23)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (66, N'Märta', 11)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (67, N'Maïlys', 30)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (68, N'Andréa', 40)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (69, N'Göran', 36)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (70, N'Gösta', 32)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (71, N'Joséphine', 16)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (72, N'Cléa', 30)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (73, N'Maëlla', 2)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (74, N'Laurène', 18)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (75, N'Cléa', 15)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (76, N'Estée', 19)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (77, N'Edmée', 14)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (78, N'Torbjörn', 4)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (79, N'Angèle', 17)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (80, N'Anaëlle', 39)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (81, N'Personnalisée', 27)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (82, N'Yú', 6)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (83, N'Cunénde', 38)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (84, N'Thérèse', 33)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (85, N'Marie-josée', 25)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (86, N'Mà', 33)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (87, N'Lóng', 37)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (88, N'Méthode', 38)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (89, N'Maëlys', 29)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (90, N'Tú', 29)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (91, N'Solène', 21)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (92, N'Lorène', 22)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (93, N'Mégane', 34)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (94, N'Laurène', 17)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (95, N'Hélène', 14)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (96, N'Salomé', 18)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (97, N'Yè', 35)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (98, N'Lóng', 19)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (99, N'Loïc', 14)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (100, N'Måns', 10)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (101, N'Mélanie', 39)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (102, N'Ruò', 7)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (103, N'Cloé', 10)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (104, N'Réjane', 24)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (105, N'Sòng', 12)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (106, N'Geneviève', 40)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (107, N'Mélina', 11)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (108, N'Zhì', 16)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (109, N'Anaé', 25)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (110, N'Eléa', 30)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (111, N'Chloé', 35)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (112, N'Illustrée', 35)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (113, N'Clémentine', 20)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (114, N'Eliès', 6)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (115, N'Laïla', 16)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (116, N'Loïs', 23)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (117, N'Félicie', 18)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (118, N'Clémence', 13)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (119, N'Océane', 40)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (120, N'Mélys', 7)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (121, N'Océane', 29)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (122, N'Céline', 6)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (123, N'Lóng', 28)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (124, N'Andrée', 5)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (125, N'Camélia', 22)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (126, N'Béatrice', 7)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (127, N'Åslög', 33)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (128, N'Aloïs', 16)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (129, N'Hélèna', 39)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (130, N'Rébecca', 7)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (131, N'Illustrée', 39)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (132, N'Néhémie', 36)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (133, N'Océanne', 9)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (134, N'Marlène', 3)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (135, N'Néhémie', 38)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (136, N'Anaëlle', 21)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (137, N'Annotés', 27)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (138, N'Táng', 25)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (139, N'Görel', 16)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (140, N'Maëlle', 24)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (141, N'Yénora', 23)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (142, N'Lén', 39)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (143, N'Tú', 26)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (144, N'Irène', 5)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (145, N'Dù', 6)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (146, N'Salomé', 21)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (147, N'Yú', 26)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (148, N'Åsa', 27)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (149, N'Dorothée', 13)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (150, N'Pélagie', 14)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (151, N'Méline', 37)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (152, N'Publicité', 35)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (153, N'Gaétane', 35)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (154, N'Léone', 19)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (155, N'Réjane', 18)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (156, N'Bérangère', 37)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (157, N'Yénora', 1)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (158, N'Gaétane', 12)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (159, N'Liè', 2)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (160, N'Naéva', 29)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (161, N'Adélie', 33)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (162, N'Esbjörn', 5)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (163, N'Görel', 31)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (164, N'Gisèle', 26)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (165, N'Aurélie', 5)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (166, N'Ruì', 2)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (167, N'Mén', 35)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (168, N'Maëlyss', 21)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (169, N'Göran', 19)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (170, N'Lèi', 14)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (171, N'Kù', 35)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (172, N'Zoé', 4)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (173, N'Clélia', 9)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (174, N'Crééz', 31)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (175, N'Björn', 26)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (176, N'Desirée', 27)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (177, N'Andréa', 14)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (178, N'Nadège', 13)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (179, N'Réservés', 16)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (180, N'Josée', 37)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (181, N'Béatrice', 38)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (182, N'Régine', 20)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (183, N'Océane', 12)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (184, N'Annotés', 15)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (185, N'Yáo', 38)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (186, N'Faîtes', 31)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (187, N'Mélina', 28)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (188, N'Marie-ève', 14)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (189, N'Pò', 26)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (190, N'Marie-françoise', 38)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (191, N'Sòng', 20)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (192, N'Marie-thérèse', 19)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (193, N'Renée', 30)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (194, N'Adèle', 14)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (195, N'Léana', 12)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (196, N'Marie-ève', 18)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (197, N'Renée', 4)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (198, N'Ruì', 31)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (199, N'Göran', 18)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (200, N'Maïlys', 11)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (201, N'Gisèle', 9)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (202, N'Liè', 21)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (203, N'Bécassine', 31)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (204, N'Uò', 18)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (205, N'Magdalène', 28)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (206, N'Méline', 31)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (207, N'Cloé', 15)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (208, N'Marlène', 11)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (209, N'Simplifiés', 8)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (210, N'Léana', 27)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (211, N'Marie-noël', 24)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (212, N'Intéressant', 3)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (213, N'Estée', 34)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (214, N'Dù', 26)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (215, N'Eléa', 21)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (216, N'Méghane', 11)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (217, N'Jú', 23)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (218, N'Adélie', 37)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (219, N'Méghane', 21)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (220, N'Cléopatre', 22)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (221, N'Clémence', 15)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (222, N'Lauréna', 16)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (223, N'Eloïse', 40)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (224, N'Yú', 37)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (225, N'Laurène', 20)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (226, N'Maïlis', 20)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (227, N'Béatrice', 20)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (228, N'Anaé', 34)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (229, N'Agnès', 16)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (230, N'Naëlle', 3)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (231, N'Laïla', 29)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (232, N'Laurène', 27)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (233, N'Zoé', 35)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (234, N'Börje', 29)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (235, N'Léa', 12)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (236, N'Cléa', 34)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (237, N'Clémentine', 8)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (238, N'Kévina', 22)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (239, N'Lyséa', 23)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (240, N'Laurélie', 36)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (241, N'Nadège', 22)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (242, N'Maïly', 22)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (243, N'Lyséa', 40)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (244, N'Réjane', 25)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (245, N'Gwenaëlle', 38)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (246, N'Réjane', 30)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (247, N'Lauréna', 6)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (248, N'Eléa', 38)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (249, N'Nadège', 12)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (250, N'Noémie', 11)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (251, N'Loïc', 35)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (252, N'Dà', 4)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (253, N'Styrbjörn', 5)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (254, N'Adélaïde', 24)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (255, N'Marie-françoise', 35)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (256, N'Mélina', 22)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (257, N'Méline', 10)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (258, N'Uò', 12)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (259, N'Bérénice', 26)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (260, N'Pò', 21)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (261, N'Erwéi', 22)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (262, N'Valérie', 25)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (263, N'Amélie', 10)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (264, N'Gisèle', 31)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (265, N'Judicaël', 35)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (266, N'Réjane', 27)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (267, N'Maëlla', 8)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (268, N'Maëlyss', 34)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (269, N'Léandre', 34)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (270, N'Dafnée', 31)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (271, N'Réservés', 31)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (272, N'Anaïs', 12)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (273, N'Loïs', 28)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (274, N'Yénora', 21)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (275, N'Göran', 35)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (276, N'Håkan', 13)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (277, N'Kévina', 25)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (278, N'Camélia', 7)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (279, N'Edmée', 14)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (280, N'Estée', 29)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (281, N'Maëline', 7)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (282, N'Maïly', 22)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (283, N'Félicie', 37)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (284, N'Lucrèce', 37)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (285, N'Léane', 10)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (286, N'Léonie', 30)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (287, N'Gérald', 32)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (288, N'Méryl', 31)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (289, N'Bérénice', 32)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (290, N'Rachèle', 36)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (291, N'Noémie', 10)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (292, N'Mylène', 16)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (293, N'Sélène', 33)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (294, N'Marie-hélène', 31)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (295, N'Ruì', 34)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (296, N'Hélène', 23)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (297, N'Maïté', 5)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (298, N'Eugénie', 7)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (299, N'Mélys', 22)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (300, N'Célestine', 39)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (301, N'Mélinda', 28)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (302, N'Mélys', 20)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (303, N'Marie-françoise', 9)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (304, N'Alizée', 8)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (305, N'Aurélie', 39)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (306, N'Anaé', 18)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (307, N'Edmée', 16)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (308, N'Illustrée', 16)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (309, N'Gaëlle', 3)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (310, N'Clémentine', 21)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (311, N'Göran', 36)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (312, N'Salomé', 39)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (313, N'Salomé', 23)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (314, N'Erwéi', 15)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (315, N'Nélie', 38)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (316, N'Célia', 7)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (317, N'Lén', 34)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (318, N'Vénus', 8)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (319, N'Séréna', 17)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (320, N'Méryl', 38)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (321, N'Mélys', 36)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (322, N'Chloé', 18)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (323, N'Mén', 26)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (324, N'Laurélie', 37)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (325, N'Garçon', 25)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (326, N'Maëlyss', 11)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (327, N'Marlène', 24)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (328, N'Almérinda', 33)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (329, N'Françoise', 24)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (330, N'Hélène', 33)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (331, N'Estève', 16)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (332, N'Cléa', 38)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (333, N'Magdalène', 29)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (334, N'Léana', 38)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (335, N'Börje', 23)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (336, N'Magdalène', 37)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (337, N'Eléonore', 22)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (338, N'Chloé', 26)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (339, N'Océanne', 15)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (340, N'Céline', 18)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (341, N'Andrée', 13)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (342, N'Cunénde', 6)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (343, N'Maï', 13)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (344, N'Vérane', 2)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (345, N'Félicie', 10)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (346, N'Eléonore', 8)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (347, N'Andréa', 22)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (348, N'Clémentine', 37)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (349, N'Michèle', 35)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (350, N'Publicité', 21)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (351, N'Ráo', 16)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (352, N'Josée', 6)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (353, N'Göran', 8)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (354, N'Léane', 21)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (355, N'Cécilia', 30)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (356, N'Börje', 34)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (357, N'Anaïs', 16)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (358, N'Cléopatre', 13)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (359, N'Lóng', 38)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (360, N'Rachèle', 24)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (361, N'Anaël', 7)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (362, N'Estève', 13)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (363, N'Agnès', 6)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (364, N'Bérangère', 11)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (365, N'Uò', 6)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (366, N'Béatrice', 25)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (367, N'Maëlyss', 26)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (368, N'Fèi', 39)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (369, N'Anaïs', 4)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (370, N'Naéva', 21)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (371, N'Stévina', 24)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (372, N'Léana', 22)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (373, N'Gaétane', 9)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (374, N'Crééz', 40)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (375, N'Andrée', 5)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (376, N'Cécilia', 3)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (377, N'Garçon', 27)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (378, N'Erwéi', 11)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (379, N'Mahélie', 9)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (380, N'Léonore', 4)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (381, N'Yóu', 38)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (382, N'Stéphanie', 29)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (383, N'Ruò', 21)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (384, N'Faîtes', 11)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (385, N'Torbjörn', 32)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (386, N'Marie-ève', 32)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (387, N'Marie-thérèse', 37)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (388, N'Joséphine', 27)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (389, N'Réjane', 1)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (390, N'Joséphine', 16)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (391, N'Lauréna', 10)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (392, N'Léonie', 32)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (393, N'Maëline', 16)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (394, N'Östen', 32)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (395, N'Cécilia', 6)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (396, N'Geneviève', 19)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (397, N'Börje', 16)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (398, N'Thérèse', 4)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (399, N'Mélanie', 39)

INSERT [dbo].[Persons] ([Id], [Name], [StarshipId]) VALUES (400, N'Lucrèce', 20)

SET IDENTITY_INSERT [dbo].[Persons] OFF

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (1, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (1, 7, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (1, 9, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (2, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (2, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (2, 5, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (2, 8, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (3, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (3, 2, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (3, 7, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (4, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (4, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (5, 1, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (5, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (5, 1, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (5, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (6, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (6, 4, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (6, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (6, 7, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (6, 8, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (7, 3, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (7, 2, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (7, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (7, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (7, 8, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (8, 1, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (8, 7, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (8, 7, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (9, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (9, 10, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (10, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (11, 10, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (11, 1, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (11, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (12, 8, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (12, 1, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (12, 1, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (13, 1, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (13, 2, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (13, 4, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (14, 3, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (14, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (14, 9, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (15, 5, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (15, 4, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (15, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (15, 10, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (15, 5, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (15, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (16, 4, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (16, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (16, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (16, 7, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (16, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (17, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (17, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (17, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (18, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (18, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (18, 2, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (18, 3, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (19, 6, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (19, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (20, 8, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (20, 6, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (20, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (20, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (21, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (21, 3, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (21, 2, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (22, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (22, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (23, 9, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (24, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (24, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (24, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (25, 9, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (25, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (25, 10, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (25, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (26, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (26, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (26, 9, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (27, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (27, 1, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (28, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (28, 9, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (28, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (29, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (29, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (30, 4, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (31, 2, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (31, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (32, 10, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (32, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (32, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (32, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (33, 6, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (33, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (33, 6, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (33, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (34, 7, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (34, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (35, 4, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (35, 6, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (35, 5, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (35, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (35, 4, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (36, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (37, 7, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (37, 4, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (38, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (38, 4, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (39, 3, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (39, 2, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (39, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (39, 1, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (40, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (40, 8, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (40, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (40, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (40, 10, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (41, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (41, 7, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (41, 4, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (42, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (42, 7, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (42, 6, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (42, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (43, 2, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (43, 6, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (43, 10, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (43, 7, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (44, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (44, 6, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (44, 10, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (45, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (45, 6, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (45, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (46, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (46, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (46, 9, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (47, 1, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (47, 1, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (47, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (47, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (47, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (48, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (48, 2, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (48, 5, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (48, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (49, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (50, 5, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (50, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (50, 1, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (50, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (51, 6, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (51, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (51, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (51, 10, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (51, 3, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (51, 10, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (52, 5, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (52, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (52, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (52, 6, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (53, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (53, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (53, 3, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (53, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (53, 9, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (54, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (54, 3, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (54, 10, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (54, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (55, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (55, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (55, 9, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (55, 7, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (56, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (56, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (56, 10, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (56, 10, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (57, 5, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (57, 10, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (57, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (58, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (58, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (58, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (59, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (59, 4, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (59, 10, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (59, 3, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (60, 8, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (60, 9, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (60, 6, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (60, 1, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (61, 1, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (61, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (61, 9, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (61, 7, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (62, 5, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (62, 9, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (63, 2, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (63, 1, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (63, 7, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (63, 4, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (64, 3, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (64, 2, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (64, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (65, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (65, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (65, 10, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (66, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (66, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (66, 10, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (67, 4, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (67, 1, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (67, 9, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (68, 3, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (68, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (69, 4, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (69, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (70, 10, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (70, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (70, 7, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (71, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (71, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (71, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (71, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (71, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (72, 6, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (72, 3, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (73, 5, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (73, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (73, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (73, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (73, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (73, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (74, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (74, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (74, 9, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (74, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (75, 4, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (75, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (75, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (75, 7, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (76, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (76, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (76, 1, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (77, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (77, 8, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (77, 9, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (77, 1, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (77, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (78, 5, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (78, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (79, 1, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (79, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (80, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (80, 9, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (80, 4, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (81, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (81, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (81, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (82, 2, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (82, 9, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (82, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (83, 6, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (83, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (83, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (83, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (83, 9, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (84, 4, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (84, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (84, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (85, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (85, 9, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (85, 3, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (86, 8, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (86, 2, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (86, 10, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (86, 10, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (87, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (87, 1, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (87, 9, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (87, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (87, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (87, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (87, 9, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (88, 3, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (88, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (88, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (88, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (89, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (89, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (90, 5, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (90, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (90, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (90, 3, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (91, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (91, 9, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (91, 6, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (91, 3, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (92, 10, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (93, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (93, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (94, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (94, 1, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (94, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (94, 1, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (94, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (95, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (95, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (95, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (96, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (96, 9, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (96, 6, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (96, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (97, 7, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (97, 6, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (97, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (98, 7, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (98, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (99, 8, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (99, 9, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (99, 3, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (99, 1, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (100, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (100, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (100, 10, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (100, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (101, 6, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (101, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (101, 9, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (102, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (102, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (102, 9, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (102, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (102, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (103, 7, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (103, 4, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (103, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (103, 2, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (104, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (104, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (104, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (105, 8, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (105, 6, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (105, 1, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (106, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (106, 2, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (106, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (106, 10, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (107, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (107, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (107, 1, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (108, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (109, 8, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (109, 8, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (109, 4, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (109, 9, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (109, 1, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (110, 7, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (110, 9, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (110, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (111, 5, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (111, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (111, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (111, 1, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (112, 1, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (112, 1, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (113, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (113, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (113, 10, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (113, 9, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (114, 8, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (114, 10, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (114, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (115, 3, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (115, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (115, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (116, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (116, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (116, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (116, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (117, 2, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (117, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (117, 1, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (117, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (117, 6, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (118, 7, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (118, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (118, 8, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (118, 9, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (118, 7, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (118, 7, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (119, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (119, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (120, 3, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (120, 3, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (120, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (120, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (121, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (121, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (121, 9, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (121, 3, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (122, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (122, 6, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (122, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (123, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (123, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (123, 6, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (124, 7, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (124, 2, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (124, 1, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (125, 4, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (125, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (126, 7, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (126, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (126, 10, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (127, 3, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (127, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (127, 10, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (128, 4, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (128, 3, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (128, 2, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (129, 9, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (129, 7, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (130, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (130, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (130, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (130, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (131, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (131, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (131, 6, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (132, 5, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (132, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (132, 8, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (132, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (132, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (132, 3, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (133, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (133, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (134, 7, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (134, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (134, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (135, 7, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (135, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (135, 10, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (135, 6, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (135, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (135, 8, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (136, 6, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (136, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (136, 1, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (137, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (137, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (137, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (137, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (137, 1, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (138, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (138, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (138, 6, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (139, 9, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (139, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (139, 1, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (139, 1, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (140, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (140, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (140, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (140, 6, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (141, 4, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (141, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (142, 9, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (142, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (142, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (143, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (143, 7, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (143, 8, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (143, 2, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (143, 4, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (144, 6, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (144, 1, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (144, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (144, 7, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (145, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (145, 10, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (145, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (145, 7, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (145, 6, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (145, 8, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (146, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (146, 5, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (147, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (147, 9, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (147, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (148, 6, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (148, 3, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (148, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (148, 5, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (148, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (150, 1, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (150, 10, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (150, 2, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (150, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (151, 5, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (151, 9, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (151, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (151, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (152, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (152, 9, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (152, 10, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (152, 7, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (152, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (152, 3, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (153, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (153, 3, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (153, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (153, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (154, 10, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (154, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (154, 8, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (155, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (155, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (155, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (155, 1, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (156, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (156, 2, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (156, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (156, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (156, 2, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (157, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (157, 7, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (157, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (157, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (157, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (158, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (158, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (158, 9, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (158, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (158, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (159, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (159, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (159, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (159, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (159, 10, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (160, 2, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (160, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (160, 1, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (160, 8, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (161, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (161, 8, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (161, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (161, 9, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (161, 3, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (162, 2, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (162, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (163, 6, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (163, 4, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (163, 9, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (163, 2, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (163, 1, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (164, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (164, 8, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (164, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (164, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (165, 4, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (165, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (165, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (165, 8, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (166, 5, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (166, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (166, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (166, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (166, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (166, 1, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (167, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (168, 7, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (168, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (168, 10, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (169, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (169, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (169, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (171, 6, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (171, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (171, 10, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (171, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (172, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (172, 10, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (172, 3, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (172, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (172, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (173, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (173, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (173, 6, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (174, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (174, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (174, 1, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (175, 9, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (175, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (175, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (176, 9, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (176, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (176, 3, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (177, 4, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (177, 3, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (177, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (177, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (178, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (178, 2, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (178, 9, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (178, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (179, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (179, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (180, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (180, 9, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (180, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (180, 10, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (180, 6, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (180, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (180, 1, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (181, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (181, 1, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (181, 2, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (181, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (182, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (182, 6, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (182, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (183, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (183, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (183, 6, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (183, 10, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (184, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (184, 1, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (184, 3, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (184, 6, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (184, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (184, 3, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (185, 5, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (185, 4, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (185, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (185, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (186, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (186, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (186, 8, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (187, 10, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (187, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (188, 4, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (188, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (188, 5, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (188, 1, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (188, 3, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (189, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (189, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (189, 6, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (190, 8, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (190, 4, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (190, 8, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (191, 6, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (191, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (191, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (191, 9, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (192, 8, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (192, 4, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (193, 7, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (193, 4, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (193, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (193, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (193, 3, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (194, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (194, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (194, 6, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (194, 6, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (195, 3, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (195, 2, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (195, 9, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (195, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (195, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (195, 10, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (196, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (196, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (196, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (196, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (197, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (197, 3, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (197, 2, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (197, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (197, 3, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (198, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (198, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (198, 3, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (199, 4, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (199, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (199, 9, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (200, 7, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (200, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (200, 9, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (200, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (200, 5, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (201, 9, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (201, 7, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (202, 3, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (202, 9, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (202, 5, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (202, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (203, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (203, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (203, 4, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (203, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (203, 10, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (203, 9, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (204, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (204, 8, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (204, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (205, 6, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (205, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (205, 3, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (206, 3, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (206, 2, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (206, 10, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (206, 3, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (207, 4, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (207, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (207, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (207, 6, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (207, 9, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (208, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (208, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (208, 3, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (209, 5, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (209, 1, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (209, 1, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (210, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (210, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (211, 3, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (211, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (211, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (211, 6, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (211, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (211, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (211, 8, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (212, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (212, 9, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (212, 6, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (212, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (213, 9, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (213, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (213, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (214, 1, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (214, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (214, 7, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (214, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (215, 4, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (215, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (215, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (215, 5, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (215, 2, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (215, 8, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (216, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (216, 3, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (216, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (216, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (217, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (218, 3, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (218, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (218, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (219, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (219, 9, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (219, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (219, 5, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (220, 9, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (220, 2, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (220, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (220, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (220, 1, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (221, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (221, 3, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (221, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (222, 8, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (222, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (222, 4, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (222, 1, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (222, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (222, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (223, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (223, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (223, 10, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (223, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (224, 4, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (224, 7, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (224, 6, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (224, 7, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (225, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (225, 3, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (225, 10, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (226, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (226, 6, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (226, 2, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (226, 6, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (227, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (227, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (227, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (227, 3, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (228, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (228, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (229, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (229, 10, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (229, 3, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (229, 6, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (230, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (230, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (230, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (230, 8, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (231, 3, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (231, 10, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (231, 1, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (232, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (232, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (233, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (233, 1, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (233, 8, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (234, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (234, 10, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (234, 7, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (235, 8, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (235, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (235, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (235, 2, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (236, 10, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (236, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (236, 2, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (236, 1, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (237, 8, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (237, 9, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (238, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (238, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (238, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (239, 5, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (239, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (239, 10, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (239, 7, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (239, 8, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (240, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (240, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (240, 9, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (241, 8, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (241, 8, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (241, 6, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (241, 3, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (241, 3, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (242, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (242, 6, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (243, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (243, 1, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (244, 4, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (244, 10, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (244, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (244, 10, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (244, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (245, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (245, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (245, 3, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (246, 4, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (246, 3, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (246, 1, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (246, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (246, 2, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (246, 8, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (247, 10, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (247, 3, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (247, 10, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (248, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (248, 2, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (248, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (248, 2, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (248, 4, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (249, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (249, 1, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (249, 4, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (250, 7, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (250, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (250, 5, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (251, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (251, 10, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (251, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (251, 8, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (252, 4, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (252, 10, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (252, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (253, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (253, 10, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (253, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (254, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (254, 8, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (254, 10, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (255, 1, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (255, 1, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (255, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (255, 9, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (256, 4, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (256, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (256, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (257, 9, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (257, 9, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (258, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (258, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (258, 1, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (258, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (258, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (259, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (259, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (259, 9, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (260, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (260, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (260, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (260, 9, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (261, 4, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (261, 9, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (261, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (261, 6, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (261, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (261, 2, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (261, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (262, 8, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (262, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (262, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (263, 6, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (263, 8, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (263, 1, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (263, 10, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (263, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (263, 3, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (263, 4, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (264, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (264, 3, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (264, 4, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (265, 6, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (265, 9, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (265, 6, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (265, 6, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (265, 6, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (266, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (266, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (266, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (267, 2, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (267, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (267, 10, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (268, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (268, 9, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (268, 4, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (268, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (268, 10, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (269, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (269, 10, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (269, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (269, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (270, 3, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (270, 1, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (270, 3, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (271, 4, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (271, 3, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (271, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (271, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (271, 2, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (271, 9, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (273, 3, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (273, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (273, 5, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (273, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (274, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (274, 9, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (274, 3, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (274, 2, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (275, 6, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (275, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (276, 1, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (276, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (276, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (276, 9, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (276, 3, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (277, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (277, 3, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (277, 2, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (277, 10, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (278, 5, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (278, 9, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (278, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (278, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (279, 7, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (279, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (279, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (280, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (280, 10, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (281, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (281, 6, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (281, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (281, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (282, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (282, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (282, 7, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (283, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (283, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (283, 7, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (283, 9, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (284, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (284, 6, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (285, 7, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (285, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (285, 1, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (285, 5, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (285, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (286, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (286, 9, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (286, 10, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (287, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (287, 1, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (287, 10, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (287, 7, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (288, 6, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (288, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (288, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (289, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (289, 3, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (289, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (290, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (290, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (290, 10, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (291, 6, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (291, 5, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (291, 4, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (292, 4, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (292, 8, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (292, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (292, 1, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (292, 10, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (293, 6, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (294, 8, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (294, 8, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (294, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (295, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (296, 10, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (296, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (296, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (297, 2, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (297, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (297, 10, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (297, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (298, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (298, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (298, 7, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (299, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (300, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (300, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (300, 2, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (300, 4, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (301, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (301, 10, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (301, 1, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (301, 7, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (301, 1, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (301, 8, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (302, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (302, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (302, 3, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (303, 4, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (303, 10, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (303, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (304, 9, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (305, 3, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (305, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (305, 5, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (305, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (306, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (306, 7, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (306, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (307, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (307, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (307, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (308, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (308, 6, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (308, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (308, 1, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (309, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (309, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (309, 3, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (309, 4, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (310, 7, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (310, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (310, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (311, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (311, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (311, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (311, 9, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (312, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (312, 8, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (312, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (312, 3, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (313, 6, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (313, 6, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (313, 10, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (314, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (314, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (314, 3, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (314, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (314, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (315, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (315, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (315, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (316, 8, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (316, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (316, 3, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (317, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (317, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (317, 1, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (317, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (317, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (317, 10, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (318, 9, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (318, 2, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (318, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (319, 6, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (319, 10, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (319, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (319, 2, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (320, 3, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (320, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (321, 4, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (321, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (321, 9, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (322, 10, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (322, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (323, 7, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (323, 2, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (323, 3, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (324, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (324, 3, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (324, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (325, 1, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (325, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (325, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (325, 2, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (325, 2, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (326, 1, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (326, 8, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (326, 9, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (326, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (326, 10, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (327, 6, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (327, 2, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (328, 8, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (328, 2, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (328, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (328, 9, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (328, 10, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (328, 2, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (328, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (329, 4, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (329, 2, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (329, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (329, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (330, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (330, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (330, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (331, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (331, 4, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (331, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (331, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (332, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (332, 3, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (332, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (332, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (333, 6, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (333, 1, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (334, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (334, 6, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (334, 3, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (334, 10, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (334, 3, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (334, 9, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (335, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (335, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (336, 2, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (336, 6, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (336, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (336, 3, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (337, 6, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (337, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (337, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (337, 6, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (337, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (338, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (338, 1, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (338, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (338, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (338, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (338, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (339, 8, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (339, 7, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (340, 4, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (340, 2, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (341, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (341, 2, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (341, 6, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (341, 4, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (341, 9, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (342, 4, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (342, 1, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (342, 10, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (342, 2, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (342, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (343, 6, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (344, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (344, 7, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (344, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (344, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (345, 10, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (346, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (346, 7, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (346, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (346, 10, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (346, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (346, 10, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (347, 8, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (347, 4, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (347, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (347, 3, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (348, 8, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (348, 2, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (348, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (348, 1, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (348, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (349, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (349, 9, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (349, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (349, 6, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (349, 5, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (350, 7, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (350, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (350, 7, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (351, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (351, 7, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (351, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (352, 5, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (352, 3, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (352, 4, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (353, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (353, 8, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (353, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (353, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (354, 3, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (354, 7, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (354, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (355, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (355, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (356, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (356, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (357, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (357, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (358, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (358, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (359, 8, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (360, 10, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (360, 8, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (360, 9, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (360, 2, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (360, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (360, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (360, 9, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (361, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (361, 8, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (361, 10, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (362, 3, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (362, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (363, 7, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (363, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (363, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (364, 1, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (364, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (364, 1, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (364, 7, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (365, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (365, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (365, 10, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (365, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (365, 4, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (366, 8, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (366, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (366, 9, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (367, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (367, 8, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (367, 10, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (367, 10, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (367, 7, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (368, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (368, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (368, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (368, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (368, 2, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (369, 5, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (369, 7, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (369, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (369, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (369, 1, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (370, 10, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (370, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (370, 4, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (370, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (370, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (371, 5, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (371, 3, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (371, 10, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (372, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (372, 8, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (372, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (373, 1, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (373, 1, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (373, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (373, 6, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (373, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (374, 8, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (374, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (374, 10, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (374, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (375, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (375, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (375, 6, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (376, 8, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (376, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (376, 1, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (377, 1, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (377, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (377, 4, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (378, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (378, 10, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (379, 8, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (379, 6, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (379, 7, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (379, 7, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (380, 9, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (381, 6, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (381, 6, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (381, 5, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (381, 7, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (381, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (382, 10, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (382, 7, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (382, 9, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (383, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (383, 6, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (383, 2, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (383, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (383, 5, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (383, 6, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (384, 6, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (384, 8, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (384, 4, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (384, 2, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (384, 9, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (385, 6, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (385, 8, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (385, 6, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (385, 10, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (385, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (386, 3, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (386, 10, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (386, 4, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (386, 5, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (386, 4, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (387, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (387, 9, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (387, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (387, 9, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (387, 10, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (388, 2, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (388, 10, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (388, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (388, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (388, 10, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (389, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (389, 7, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (389, 10, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (390, 9, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (390, 8, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (390, 4, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (391, 5, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (391, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (391, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (391, 8, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (391, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (392, 6, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (392, 5, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (392, 9, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (392, 1, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (392, 8, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (393, 2, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (393, 9, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (393, 8, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (394, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (394, 4, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (394, 6, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (394, 7, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (394, 10, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (395, 9, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (395, 8, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (395, 1, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (395, 8, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (396, 6, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (396, 4, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (396, 10, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (397, 5, 1)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (397, 5, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (397, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (397, 10, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (398, 9, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (398, 3, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (398, 6, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (398, 4, 7)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (399, 6, 2)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (399, 8, 3)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (399, 3, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (399, 7, 6)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (400, 1, 4)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (400, 10, 5)

INSERT [dbo].[PersonToPersonAttribute] ([PersonId], [Value], [PersonAttributeId]) VALUES (400, 7, 7)

SET IDENTITY_INSERT [dbo].[StarshipAttributes] ON 

INSERT [dbo].[StarshipAttributes] ([Id], [Name]) VALUES (1, N'Machine Guns')

INSERT [dbo].[StarshipAttributes] ([Id], [Name]) VALUES (2, N'Plasma Guns')

INSERT [dbo].[StarshipAttributes] ([Id], [Name]) VALUES (3, N'Light Coilgun')

INSERT [dbo].[StarshipAttributes] ([Id], [Name]) VALUES (4, N'Heavy Coilgun')

INSERT [dbo].[StarshipAttributes] ([Id], [Name]) VALUES (5, N'Light Laser Gun')

INSERT [dbo].[StarshipAttributes] ([Id], [Name]) VALUES (6, N'Heavy Laser Gun')

INSERT [dbo].[StarshipAttributes] ([Id], [Name]) VALUES (7, N'Basic Protective Coating')

INSERT [dbo].[StarshipAttributes] ([Id], [Name]) VALUES (8, N'Extended Protective Coating')

SET IDENTITY_INSERT [dbo].[StarshipAttributes] OFF

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (1, 8, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (1, 5, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (2, 1, 1)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (2, 8, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (2, 6, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (2, 9, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (3, 10, 1)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (3, 4, 2)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (3, 10, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (3, 9, 5)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (3, 8, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (3, 8, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (3, 8, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (4, 4, 2)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (4, 3, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (4, 4, 5)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (4, 10, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (4, 3, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (5, 5, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (5, 5, 4)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (5, 3, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (5, 9, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (6, 9, 2)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (6, 9, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (6, 3, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (6, 2, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (6, 8, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (7, 2, 2)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (7, 6, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (7, 3, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (7, 1, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (7, 5, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (8, 9, 2)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (8, 3, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (8, 2, 4)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (8, 9, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (8, 3, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (9, 7, 1)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (9, 2, 2)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (9, 6, 4)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (9, 8, 5)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (9, 8, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (9, 6, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (10, 4, 1)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (10, 9, 2)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (10, 9, 4)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (10, 4, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (10, 9, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (11, 3, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (11, 5, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (11, 1, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (12, 3, 2)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (12, 5, 5)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (12, 4, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (13, 10, 1)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (13, 7, 2)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (13, 4, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (13, 9, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (13, 2, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (14, 10, 1)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (14, 10, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (14, 7, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (15, 1, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (15, 9, 4)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (15, 4, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (15, 10, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (16, 1, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (16, 6, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (16, 5, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (17, 2, 4)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (17, 3, 5)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (17, 1, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (18, 9, 2)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (18, 8, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (18, 8, 4)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (18, 9, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (19, 4, 1)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (19, 4, 4)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (19, 3, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (19, 1, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (19, 1, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (20, 10, 4)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (20, 1, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (21, 10, 2)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (21, 2, 5)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (21, 9, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (21, 2, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (22, 7, 1)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (22, 7, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (22, 7, 4)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (22, 4, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (22, 6, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (22, 7, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (23, 3, 2)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (23, 2, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (23, 2, 5)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (23, 4, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (23, 8, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (24, 3, 1)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (24, 1, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (24, 5, 4)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (24, 5, 5)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (24, 9, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (25, 9, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (25, 3, 4)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (25, 10, 5)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (26, 2, 1)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (26, 2, 2)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (26, 1, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (26, 5, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (26, 1, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (27, 10, 1)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (27, 3, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (27, 6, 4)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (27, 10, 5)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (27, 2, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (27, 8, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (28, 4, 2)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (28, 10, 5)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (28, 2, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (29, 4, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (29, 9, 4)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (29, 6, 5)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (30, 1, 1)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (30, 8, 5)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (30, 8, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (30, 6, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (30, 7, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (31, 9, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (31, 4, 5)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (31, 5, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (31, 3, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (32, 7, 1)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (32, 7, 5)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (32, 6, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (32, 2, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (33, 6, 1)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (33, 1, 2)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (33, 2, 5)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (33, 1, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (34, 7, 2)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (34, 7, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (34, 1, 5)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (34, 2, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (35, 4, 1)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (35, 2, 2)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (35, 1, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (35, 10, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (35, 3, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (36, 3, 1)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (36, 3, 2)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (36, 9, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (36, 6, 5)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (36, 3, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (37, 5, 1)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (37, 2, 2)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (37, 2, 3)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (37, 4, 4)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (37, 5, 5)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (37, 2, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (37, 1, 7)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (38, 1, 2)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (38, 7, 6)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (38, 4, 8)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (39, 8, 1)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (40, 8, 4)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (40, 7, 5)

INSERT [dbo].[StarshipToStarshipAttribute] ([StarshipId], [Value], [StarshipAttributeId]) VALUES (40, 2, 6)

