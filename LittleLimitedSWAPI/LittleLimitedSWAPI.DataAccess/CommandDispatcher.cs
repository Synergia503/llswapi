﻿using Autofac;
using LittleLimitedSWAPI.Domain;
using System;
using System.Threading.Tasks;

namespace LittleLimitedSWAPI.DataAccess
{
    public class CommandDispatcher : ICommandDispatcher
    {
        private readonly IComponentContext _context;

        public CommandDispatcher(IComponentContext context)
        {
            _context = context;
        }

        public async Task DispatchCommandAsync<TCommand>(TCommand command) where TCommand : ICommand
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command), $"Command {typeof(TCommand)} can not be null.");
            }

            ICommandHandler<TCommand> commandHandler = _context.Resolve<ICommandHandler<TCommand>>();
            await commandHandler.HandleCommandAsync(command);
        }
    }
}