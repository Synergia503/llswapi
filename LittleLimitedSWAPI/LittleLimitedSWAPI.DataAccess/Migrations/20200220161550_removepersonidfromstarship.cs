﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LittleLimitedSWAPI.DataAccess.Migrations
{
    public partial class removepersonidfromstarship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PersonId",
                table: "Starships");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PersonId",
                table: "Starships",
                nullable: false,
                defaultValue: 0);
        }
    }
}
