﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LittleLimitedSWAPI.DataAccess.Migrations
{
    public partial class fixstarshipkindrelationships : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StarshipId",
                table: "StarshipKinds");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StarshipId",
                table: "StarshipKinds",
                nullable: false,
                defaultValue: 0);
        }
    }
}
