﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LittleLimitedSWAPI.DataAccess.Migrations
{
    public partial class changedrelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Starships_StarshipKindId",
                table: "Starships");

            migrationBuilder.CreateIndex(
                name: "IX_Starships_StarshipKindId",
                table: "Starships",
                column: "StarshipKindId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Starships_StarshipKindId",
                table: "Starships");

            migrationBuilder.CreateIndex(
                name: "IX_Starships_StarshipKindId",
                table: "Starships",
                column: "StarshipKindId",
                unique: true);
        }
    }
}
