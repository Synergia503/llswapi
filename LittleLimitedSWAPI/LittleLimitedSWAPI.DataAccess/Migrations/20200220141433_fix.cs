﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LittleLimitedSWAPI.DataAccess.Migrations
{
    public partial class fix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PersonAttributes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonAttributes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StarshipAttributes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StarshipAttributes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "StarshipKinds",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StarshipKindName = table.Column<string>(maxLength: 32, nullable: false),
                    StarshipId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StarshipKinds", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Starships",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 32, nullable: false),
                    StarshipKindId = table.Column<int>(nullable: false),
                    PersonId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Starships", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Starships_StarshipKinds_StarshipKindId",
                        column: x => x.StarshipKindId,
                        principalTable: "StarshipKinds",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Persons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 32, nullable: false),
                    StarshipId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Persons", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Persons_Starships_StarshipId",
                        column: x => x.StarshipId,
                        principalTable: "Starships",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StarshipToStarshipAttribute",
                columns: table => new
                {
                    StarshipId = table.Column<int>(nullable: false),
                    Value = table.Column<int>(nullable: false),
                    StarshipAttributeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StarshipToStarshipAttribute", x => new { x.StarshipId, x.StarshipAttributeId });
                    table.ForeignKey(
                        name: "FK_StarshipToStarshipAttribute_StarshipAttributes_StarshipAttributeId",
                        column: x => x.StarshipAttributeId,
                        principalTable: "StarshipAttributes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StarshipToStarshipAttribute_Starships_StarshipId",
                        column: x => x.StarshipId,
                        principalTable: "Starships",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PersonToPersonAttribute",
                columns: table => new
                {
                    PersonId = table.Column<int>(nullable: false),
                    Value = table.Column<int>(nullable: false),
                    PersonAttributeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonToPersonAttribute", x => new { x.PersonId, x.PersonAttributeId });
                    table.ForeignKey(
                        name: "FK_PersonToPersonAttribute_PersonAttributes_PersonAttributeId",
                        column: x => x.PersonAttributeId,
                        principalTable: "PersonAttributes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PersonToPersonAttribute_Persons_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Persons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Persons_StarshipId",
                table: "Persons",
                column: "StarshipId");

            migrationBuilder.CreateIndex(
                name: "IX_PersonToPersonAttribute_PersonAttributeId",
                table: "PersonToPersonAttribute",
                column: "PersonAttributeId");

            migrationBuilder.CreateIndex(
                name: "IX_Starships_StarshipKindId",
                table: "Starships",
                column: "StarshipKindId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_StarshipToStarshipAttribute_StarshipAttributeId",
                table: "StarshipToStarshipAttribute",
                column: "StarshipAttributeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PersonToPersonAttribute");

            migrationBuilder.DropTable(
                name: "StarshipToStarshipAttribute");

            migrationBuilder.DropTable(
                name: "PersonAttributes");

            migrationBuilder.DropTable(
                name: "Persons");

            migrationBuilder.DropTable(
                name: "StarshipAttributes");

            migrationBuilder.DropTable(
                name: "Starships");

            migrationBuilder.DropTable(
                name: "StarshipKinds");
        }
    }
}
