﻿using LittleLimitedSWAPI.Domain;
using System;
using System.Threading.Tasks;
using Autofac;

namespace LittleLimitedSWAPI.DataAccess
{
    public class QueryDispatcher : IQueryDispatcher
    {
        private readonly IComponentContext _context;

        public QueryDispatcher(IComponentContext context)
        {
            _context = context;
        }

        public async Task<TResult> DispatchQueryAsync<TQuery, TResult>(TQuery query) where TQuery : IQuery
        {
            if (query == null)
            {
                throw new ArgumentNullException(nameof(query), $"Query {typeof(TQuery)} can not be null.");
            }

            IQueryHandler<TQuery, TResult> queryHandler = _context.Resolve<IQueryHandler<TQuery, TResult>>();
            return await queryHandler.HandleQuery(query);
        }
    }
}