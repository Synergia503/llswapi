﻿namespace LittleLimitedSWAPI.Domain.Paging
{
    public interface IHasPaging
    {
        int PageSize { get; }
        int PageNumber { get; }
    }
}