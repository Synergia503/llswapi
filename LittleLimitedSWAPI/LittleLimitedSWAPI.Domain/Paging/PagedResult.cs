﻿using System.Collections.Generic;

namespace LittleLimitedSWAPI.Domain.Paging
{
    public class PagedResult<T>
    {
        public int TotalCount { get; set; }
        public List<T> Items { get; set; }
    }
}