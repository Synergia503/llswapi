﻿namespace LittleLimitedSWAPI.Domain.Paging
{
    public static class PagingExtensions
    {
        public static Page MapPageableQueryToPage(this IHasPaging query)
            => new Page(query.PageNumber, query.PageSize);
    }
}