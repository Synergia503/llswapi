﻿using System.Linq;
using System.Threading.Tasks;

namespace LittleLimitedSWAPI.Domain.Paging
{
    public interface IPagedListQuery<TPageableEntity> where TPageableEntity : IPageableEntity
    {
        Task<PagedResult<TPageableEntity>> GetPagedResult(IQueryable<TPageableEntity> query, Page page);
    }
}