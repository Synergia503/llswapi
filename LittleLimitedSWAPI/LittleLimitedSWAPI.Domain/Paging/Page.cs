﻿namespace LittleLimitedSWAPI.Domain.Paging
{
    public class Page
    {
        public Page(int number, int size)
        {
            Size = size;
            Number = number;
        }

        public int Size { get; }
        public int Number { get; }
    }
}