﻿namespace LittleLimitedSWAPI.Domain.People.Queries
{
    public class GetPersonById : IQuery
    {
        public int PersonId { get; set; }
    }
}