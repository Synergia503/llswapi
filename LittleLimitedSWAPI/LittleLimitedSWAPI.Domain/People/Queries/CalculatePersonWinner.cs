﻿namespace LittleLimitedSWAPI.Domain.People.Queries
{
    public class CalculatePersonWinner : IQuery
    {
        public int AttackerPersonId { get; set; }
        public int DefenderPersonId { get; set; }
        public int CrucialPersonAttributeId { get; set; }
    }
}