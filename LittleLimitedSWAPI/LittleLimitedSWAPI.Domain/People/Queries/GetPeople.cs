﻿using LittleLimitedSWAPI.Domain.Paging;

namespace LittleLimitedSWAPI.Domain.People.Queries
{
    public class GetPeople : IQuery, IHasPaging
    {
        public GetPeople()
        {

        }

        public int PageSize { get; set; }
        public int PageNumber { get; set; }
    }
}