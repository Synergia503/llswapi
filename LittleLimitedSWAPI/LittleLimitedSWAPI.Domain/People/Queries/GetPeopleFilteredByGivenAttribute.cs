﻿using LittleLimitedSWAPI.Domain.Paging;

namespace LittleLimitedSWAPI.Domain.People.Queries
{
    public class GetPeopleFilteredByGivenAttribute : IQuery, IHasPaging
    {
        public GetPeopleFilteredByGivenAttribute()
        {
        }

        public int PageSize { get; private set; }
        public int PageNumber { get; private set; }
        public int CommonAttributeId { get; set; }
    }
}