﻿using LittleLimitedSWAPI.Domain.Starships;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LittleLimitedSWAPI.Domain.People
{
    public class Person : IPageableEntity
    {
        protected Person()
        {

        }

        private Person(string name, List<PersonAttribute> personAttributes)
        {
            SetName(name);
            SetPersonAttributes(personAttributes);
        }

        public int Id { get; private set; }
        public string Name { get; private set; }
        public List<PersonToPersonAttribute> PersonToPersonAttributes { get; private set; }
        public Starship Starship { get; private set; }
        public int StarshipId { get; private set; }

        public static Person Create(string name, List<PersonAttribute> personAttributes)
            => new Person(name, personAttributes);

        public void SetName(string name)
            => Name = string.IsNullOrEmpty(name) ? throw new Exception() : name;

        public void SetPersonAttributes(List<PersonAttribute> personAttributes)
        {
            if (personAttributes is null)
            {
                throw new Exception("Person attributes must not be null");
            }

            PersonToPersonAttributes = personAttributes
                .Select(pa => PersonToPersonAttribute.Create(this, 0, pa)) // TODO: BJ 0 should be parametrized
                .ToList();
        }
    }
}