﻿namespace LittleLimitedSWAPI.Domain.People.ReadModels
{
    public class PeopleFilteredByAttributeReadModel : IReadModel
    {
        public int PersonId { get; set; }
        public string PersonName { get; set; }
        public int PersonAttributeValue { get; set; }
    }
}