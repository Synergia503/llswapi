﻿using System.Collections.Generic;

namespace LittleLimitedSWAPI.Domain.People.ReadModels
{
    public class PersonWithAttributesReadModel : IReadModel
    {
        public int PersonId { get; set; }
        public string PersonName { get; set; }
        public List<PersonToPersonAttributeReadModel> PersonAttributes { get; set; }
    }
}