﻿namespace LittleLimitedSWAPI.Domain.People.ReadModels
{
    public class PersonReadModel : IReadModel
    {
        public int PersonId { get; set; }
        public string PersonName { get; set; }
    }
}