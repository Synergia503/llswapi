﻿namespace LittleLimitedSWAPI.Domain.People.ReadModels
{
    public class PersonToPersonAttributeReadModel : IReadModel
    {
        public int PersonAttributeId { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }

    }
}