﻿namespace LittleLimitedSWAPI.Domain.People.ReadModels
{
    public class PersonWinnerReadModel : IReadModel
    {
        public int PersonId { get; set; }
        public string Name { get; set; }
    }
}