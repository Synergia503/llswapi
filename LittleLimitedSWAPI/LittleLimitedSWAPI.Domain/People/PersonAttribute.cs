﻿using System.Collections.Generic;

namespace LittleLimitedSWAPI.Domain.People
{
    public class PersonAttribute
    {
        protected PersonAttribute()
        {

        }

        private PersonAttribute(string name)
        {
            Name = name;
        }

        public int Id { get; private set; }
        public string Name { get; private set; }
        public List<PersonToPersonAttribute> PersonToPersonAttributes { get; private set; }

        public static PersonAttribute Create(string name)
            => new PersonAttribute(name);
    }
}