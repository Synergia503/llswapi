﻿namespace LittleLimitedSWAPI.Domain.People
{
    public class PersonToPersonAttribute
    {
        protected PersonToPersonAttribute()
        {

        }

        private PersonToPersonAttribute(Person person, int value, PersonAttribute personAttribute)
        {
            Person = person;
            Value = value;
            PersonAttribute = personAttribute;
        }

        public int PersonId { get; private set; }
        public Person Person { get; private set; }

        public int Value { get; private set; }

        public int PersonAttributeId { get; private set; }
        public PersonAttribute PersonAttribute { get; private set; }

        public static PersonToPersonAttribute Create(Person person, int value, PersonAttribute personAttribute)
            => new PersonToPersonAttribute(person, value, personAttribute);


        public static PersonToPersonAttribute operator >(PersonToPersonAttribute person1, PersonToPersonAttribute person2)
        {
            return person1.Value > person2.Value ? person1 : person2;
        }

        public static PersonToPersonAttribute operator <(PersonToPersonAttribute person1, PersonToPersonAttribute person2)
        {
            return person1.Value < person2.Value ? person1 : person2;
        }
    }
}