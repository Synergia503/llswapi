﻿namespace LittleLimitedSWAPI.Domain
{
    public interface IPageableEntity
    {
        int Id { get; }
    }
}