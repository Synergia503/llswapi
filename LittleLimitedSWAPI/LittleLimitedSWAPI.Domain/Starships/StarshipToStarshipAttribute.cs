﻿namespace LittleLimitedSWAPI.Domain.Starships
{
    public class StarshipToStarshipAttribute
    {
        protected StarshipToStarshipAttribute()
        {

        }

        private StarshipToStarshipAttribute(Starship starship, int value, StarshipAttribute starshipAttribute)
        {
            Starship = starship;
            Value = value;
            StarshipAttribute = starshipAttribute;
        }

        public int StarshipId { get; private set; }
        public Starship Starship { get; private set; }

        public int Value { get; private set; }

        public int StarshipAttributeId { get; private set; }
        public StarshipAttribute StarshipAttribute { get; private set; }

        public static StarshipToStarshipAttribute Create(Starship starship, int value, StarshipAttribute starshipAttribute)
            => new StarshipToStarshipAttribute(starship, value, starshipAttribute);

        public static StarshipToStarshipAttribute operator >(StarshipToStarshipAttribute starship1, StarshipToStarshipAttribute starship2)
        {
            return starship1.Value > starship2.Value ? starship1 : starship2;
        }

        public static StarshipToStarshipAttribute operator <(StarshipToStarshipAttribute starship1, StarshipToStarshipAttribute starship2)
        {
            return starship1.Value < starship2.Value ? starship1 : starship2;
        }
    }
}