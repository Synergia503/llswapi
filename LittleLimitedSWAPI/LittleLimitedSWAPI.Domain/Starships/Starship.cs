﻿using LittleLimitedSWAPI.Domain.People;
using System.Collections.Generic;
using System.Linq;

namespace LittleLimitedSWAPI.Domain.Starships
{
    public class Starship : IPageableEntity
    {
        protected Starship()
        {

        }

        private Starship(string name, List<Person> people, StarshipKind starshipKind, List<StarshipAttribute> starshipAttributes)
        {
            Name = name;
            People = people;
            StarshipKind = starshipKind;
            StarshipKindId = starshipKind.Id;
            StarshipToStarshipAttributes = starshipAttributes?
                .Select(sa => StarshipToStarshipAttribute.Create(this, 0, sa)) // TODO: 0 should be parametrized
                .ToList();
        }

        public int Id { get; private set; }
        public string Name { get; private set; }
        public List<Person> People { get; private set; }
        public List<StarshipToStarshipAttribute> StarshipToStarshipAttributes { get; private set; }
        public StarshipKind StarshipKind { get; private set; }
        public int StarshipKindId { get; private set; }

        public static Starship Create(string name, List<Person> people, StarshipKind starshipKind, List<StarshipAttribute> starshipAttributes)
            => new Starship(name, people, starshipKind, starshipAttributes);
    }
}