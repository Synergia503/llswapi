﻿using LittleLimitedSWAPI.Domain.Paging;

namespace LittleLimitedSWAPI.Domain.Starships.Queries
{
    public class GetStarships : IQuery, IHasPaging
    {
        public GetStarships()
        {

        }

        public int PageSize { get; set; }
        public int PageNumber { get; set; }
    }
}