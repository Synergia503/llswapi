﻿using LittleLimitedSWAPI.Domain.Paging;

namespace LittleLimitedSWAPI.Domain.Starships.Queries
{
    public class GetStarshipsFilteredByGivenAttribute : IQuery, IHasPaging
    {
        public GetStarshipsFilteredByGivenAttribute()
        {

        }

        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int CommonAttributeId { get; set; }
    }
}