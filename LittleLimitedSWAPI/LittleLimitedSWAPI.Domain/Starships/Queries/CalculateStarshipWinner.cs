﻿namespace LittleLimitedSWAPI.Domain.Starships.Queries
{
    public class CalculateStarshipWinner : IQuery
    {
        public int AttackerStarshipId { get; set; }
        public int DefenderStarshipId { get; set; }
        public int CrucialStarshipAttributeId { get; set; }
    }
}