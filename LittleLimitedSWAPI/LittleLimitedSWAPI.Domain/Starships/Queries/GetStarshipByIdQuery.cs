﻿namespace LittleLimitedSWAPI.Domain.Starships.Queries
{
    public class GetStarshipById : IQuery
    {
        public int StarshipId { get; set; }
    }
}