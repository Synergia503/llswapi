﻿using System.Collections.Generic;

namespace LittleLimitedSWAPI.Domain.Starships.ReadModels
{
    public class StarshipWithAttributesReadModel : IReadModel
    {
        public int StarshipId { get; set; }
        public string StarshipName { get; set; }
        public List<StarshipToStarshipAttributeReadModel> StarshipAttributes { get; set; }
    }
}