﻿namespace LittleLimitedSWAPI.Domain.Starships.ReadModels
{
    public class StarshipWinnerReadModel : IReadModel
    {
        public int StarshipId { get; set; }
        public string Name { get; set; }
    }
}