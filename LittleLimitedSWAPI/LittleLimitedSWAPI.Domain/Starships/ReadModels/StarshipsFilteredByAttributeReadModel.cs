﻿namespace LittleLimitedSWAPI.Domain.Starships.ReadModels
{
    public class StarshipsFilteredByAttributeReadModel : IReadModel
    {
        public int StarshipId { get; set; }
        public string StarshipName { get; set; }
        public int StarshipAttributeValue { get; set; }
    }
}