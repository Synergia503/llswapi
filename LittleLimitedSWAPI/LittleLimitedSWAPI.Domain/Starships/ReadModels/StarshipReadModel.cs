﻿namespace LittleLimitedSWAPI.Domain.Starships.ReadModels
{
    public class StarshipReadModel : IReadModel
    {
        public int StarshipId { get; set; }
        public string StarshipName { get; set; }
    }
}