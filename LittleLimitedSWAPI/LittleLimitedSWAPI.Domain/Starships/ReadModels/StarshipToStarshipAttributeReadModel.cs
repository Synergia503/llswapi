﻿namespace LittleLimitedSWAPI.Domain.Starships.ReadModels
{
    public class StarshipToStarshipAttributeReadModel : IReadModel
    {
        public int StarshipAttributeId { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }

    }
}