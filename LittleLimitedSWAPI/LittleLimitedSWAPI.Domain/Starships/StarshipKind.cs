﻿using System.Collections.Generic;

namespace LittleLimitedSWAPI.Domain.Starships
{
    public class StarshipKind
    {
        protected StarshipKind()
        {

        }

        private StarshipKind(string starshipKindName, List<Starship> starships)
        {
            StarshipKindName = starshipKindName;
            Starships = starships;
        }

        public int Id { get; private set; }
        public string StarshipKindName { get; private set; }
        public List<Starship> Starships { get; private set; }

        public static StarshipKind Create(string starshipKindName, List<Starship> starships)
            => new StarshipKind(starshipKindName, starships);
    }
}