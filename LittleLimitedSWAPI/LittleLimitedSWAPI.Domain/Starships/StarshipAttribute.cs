﻿using System.Collections.Generic;
using System.Linq;

namespace LittleLimitedSWAPI.Domain.Starships
{
    public class StarshipAttribute
    {
        protected StarshipAttribute()
        {

        }

        private StarshipAttribute(
            string name,
            List<Starship> starships)
        {
            Name = name;
            StarshipToStarshipAttributes = starships?
                .Select(s => StarshipToStarshipAttribute.Create(s, 0, this))
                .ToList();
        }

        public int Id { get; private set; }
        public string Name { get; private set; }
        public List<StarshipToStarshipAttribute> StarshipToStarshipAttributes { get; private set; }

        public static StarshipAttribute Create(string name, List<Starship> starships)
            => new StarshipAttribute(name, starships);
    }
}
