﻿using System.Threading.Tasks;

namespace LittleLimitedSWAPI.Domain
{
    public interface IQueryDispatcher
    {
        Task<TResult> DispatchQueryAsync<TQuery, TResult>(TQuery query) where TQuery : IQuery;
    }
}
