﻿using System.Threading.Tasks;

namespace LittleLimitedSWAPI.Domain
{
    public interface IQueryHandler<TQuery, TResult> where TQuery : IQuery
    {
        Task<TResult> HandleQuery(TQuery query);
    }
}