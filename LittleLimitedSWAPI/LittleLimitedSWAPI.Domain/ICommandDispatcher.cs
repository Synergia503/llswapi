﻿using System.Threading.Tasks;

namespace LittleLimitedSWAPI.Domain
{
    public interface ICommandDispatcher
    {
        Task DispatchCommandAsync<TCommand>(TCommand command) where TCommand : ICommand;
    }
}