﻿using System.Threading.Tasks;

namespace LittleLimitedSWAPI.Domain
{
    public interface ICommandHandler<TCommand> where TCommand : ICommand
    {
        Task HandleCommandAsync(ICommand command);
    }
}