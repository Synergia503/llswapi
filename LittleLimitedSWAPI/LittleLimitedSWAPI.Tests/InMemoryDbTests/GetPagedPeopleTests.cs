﻿using LittleLimitedSWAPI.Domain.Paging;
using LittleLimitedSWAPI.Domain.People;
using LittleLimitedSWAPI.Domain.People.Queries;
using LittleLimitedSWAPI.Domain.People.ReadModels;
using LittleLimitedSWAPI.Tests.Fixtures;
using System.Threading.Tasks;
using Xunit;

namespace LittleLimitedSWAPI.Tests.InMemoryDbTests
{
    [Collection(nameof(PersonTestFixtureCollection))]
    public class GetPagedPeopleTests
    {
        private readonly PersonTestFixture<Person> _testFixture;

        public GetPagedPeopleTests(PersonTestFixture<Person> testFixture)
        {
            _testFixture = testFixture;
        }

        [Fact]
        public async Task should_return_proper_entities_for_given_page_number_and_page_size()
        {
            // arrange

            await _testFixture
                    .SetupDatabase()
                    .AddManyToDatabase(15)
                    .SaveChangesAsync();

            // act
            PagedResult<PersonReadModel> result = _testFixture.HandleQuery<GetPeople, PagedResult<PersonReadModel>>();
            PagedResult<PersonReadModel> expected = _testFixture.GetExpectedCollection();

            // assert

            _testFixture
                .VerifyFor(result: result, expected: expected)
                .VerifyForPage(new Page(1, 10))
                .ResultTotalCount()
                .ResultCollectionItemsCount()
                .ResultCollectionItems();
        }
    }
}