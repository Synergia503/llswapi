﻿using LittleLimitedSWAPI.Domain;
using LittleLimitedSWAPI.Domain.Paging;

namespace LittleLimitedSWAPI.Tests.Fixtures
{
    public class Verification<T> : IVerification<T> where T : IReadModel
    {
        private readonly PagedResult<T> _result;
        private readonly PagedResult<T> _expected;

        public Verification(PagedResult<T> result, PagedResult<T> expected)
        {
            _result = result;
            _expected = expected;
        }

        public IVerification<T> ResultCollectionItems()
        {
            return this;
        }

        public IVerification<T> ResultCollectionItemsCount()
        {
            return this;
        }

        public IVerification<T> ResultTotalCount()
        {
            return this;
        }

        public IVerification<T> VerifyForPage(Page page)
        {
            return this;
        }
    }

    public interface IVerification<T> where T : IReadModel
    {
        IVerification<T> ResultCollectionItems();
        IVerification<T> ResultCollectionItemsCount();
        IVerification<T> ResultTotalCount();
        IVerification<T> VerifyForPage(Page page);
    }
}