﻿using AutoFixture;
using LittleLimitedSWAPI.DataAccess.EF;
using LittleLimitedSWAPI.Domain;
using LittleLimitedSWAPI.Domain.Paging;
using LittleLimitedSWAPI.Domain.People.ReadModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleLimitedSWAPI.Tests.Fixtures
{
    public class PersonTestFixture<TEntity> : IDisposable where TEntity : class, IPageableEntity
    {
        private readonly Fixture _autoFixture = new Fixture();
        public LLContext LLContext { get; private set; }

        public List<Task> SetupTasks => new List<Task>();

        public PersonTestFixture<TEntity> SetupDatabase()
        {
            async Task actionAsync()
            {
                DbContextOptions<LLContext> options = new DbContextOptionsBuilder<LLContext>()
                    .UseInMemoryDatabase("InMemory")
                    .Options;

                LLContext = new LLContext(options);
                await LLContext.Database.EnsureCreatedAsync();
            };

            SetupTasks.Add(actionAsync());
            return this;
        }

        public IVerification<T> VerifyFor<T>(PagedResult<T> result, PagedResult<T> expected) where T : IReadModel
        {
            return new Verification<T>(result, expected);
        }

        public PagedResult<PersonReadModel> GetExpectedCollection()
        {
            return new PagedResult<PersonReadModel>();
        }

        public PagedResult<PersonReadModel> HandleQuery<TQuery, TReadModel>() where TQuery : IQuery
        {
            // TODO: BJ Get query handler from autofac context
            return new PagedResult<PersonReadModel>();
        }

        public PersonTestFixture<TEntity> AddManyToDatabase(int count)
        {
            async Task actionAsync()
            {
                List<TEntity> entitiesToBeAddedToContext = _autoFixture.CreateMany<TEntity>(count).ToList();
                await LLContext.AddRangeAsync(entitiesToBeAddedToContext);
            };

            SetupTasks.Add(actionAsync());
            return this;
        }

        public async Task SaveChangesAsync()
        {
            foreach (Task task in SetupTasks)
            {
                await task;
            }

            await LLContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            LLContext.Database.EnsureDeleted();
        }

        // TODO: BJ mapping could be done for all entities
        //private PagedResult<PersonReadModel> MapToPersonReadModel(List<Person> people)
        //{
        //    return new PagedResult<PersonReadModel>
        //    {
        //    };
        //}
    }
}