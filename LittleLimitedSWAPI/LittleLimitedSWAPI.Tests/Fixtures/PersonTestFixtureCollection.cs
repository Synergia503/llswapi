﻿using LittleLimitedSWAPI.Domain.People;
using Xunit;

namespace LittleLimitedSWAPI.Tests.Fixtures
{
    [CollectionDefinition(nameof(PersonTestFixtureCollection))]
    public class PersonTestFixtureCollection : ICollectionFixture<PersonTestFixture<Person>>
    {
    }
}