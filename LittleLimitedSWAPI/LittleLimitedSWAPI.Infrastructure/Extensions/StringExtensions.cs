﻿namespace LittleLimitedSWAPI.Infrastructure.Extensions
{
    public static class StringExtensions
    {
        public static bool ToBool(this string value, bool defaultValue)
        {
            if (string.IsNullOrEmpty(value))
                return defaultValue;

            if (!bool.TryParse(value, out bool result))
                return defaultValue;

            return result;
        }
    }
}