﻿using Autofac;
using LittleLimitedSWAPI.Domain;

namespace LittleLimitedSWAPI.Infrastructure.IoC
{
    public class DomainModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(IPageableEntity).Assembly)
                 .AsImplementedInterfaces();
        }
    }
}