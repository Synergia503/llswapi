﻿using Autofac;

namespace LittleLimitedSWAPI.Infrastructure.IoC
{
    public class ContainerModule : Autofac.Module
    {
        public ContainerModule()
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<DataAccessModule>();
            builder.RegisterModule<CommandModule>();
            builder.RegisterModule<QueryModule>();
            builder.RegisterModule<DomainModule>();
        }
    }
}