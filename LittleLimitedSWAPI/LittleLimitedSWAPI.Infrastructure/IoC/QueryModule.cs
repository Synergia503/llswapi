﻿using Autofac;
using LittleLimitedSWAPI.DataAccess;
using LittleLimitedSWAPI.Domain;
using System.Reflection;

namespace LittleLimitedSWAPI.Infrastructure.IoC
{
    public class QueryModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(QueryDispatcher).Assembly)
               .AsClosedTypesOf(typeof(IQueryHandler<,>))
               .InstancePerLifetimeScope();

            builder.RegisterType<QueryDispatcher>()
               .As<IQueryDispatcher>()
               .InstancePerLifetimeScope();
        }
    }
}