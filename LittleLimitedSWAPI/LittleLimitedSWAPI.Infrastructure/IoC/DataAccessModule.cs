﻿using Autofac;
using LittleLimitedSWAPI.DataAccess.Paging;

namespace LittleLimitedSWAPI.Infrastructure.IoC
{
    public class DataAccessModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(PagedListQuery<>))
                .AsImplementedInterfaces();
        }
    }
}