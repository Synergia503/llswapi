﻿using Autofac;
using LittleLimitedSWAPI.DataAccess;
using LittleLimitedSWAPI.Domain;

namespace LittleLimitedSWAPI.Infrastructure.IoC
{
    public class CommandModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(CommandDispatcher).Assembly)
               .AsClosedTypesOf(typeof(ICommandHandler<>))
               .InstancePerLifetimeScope();

            builder.RegisterType<CommandDispatcher>()
               .As<ICommandDispatcher>()
               .InstancePerLifetimeScope();
        }
    }
}