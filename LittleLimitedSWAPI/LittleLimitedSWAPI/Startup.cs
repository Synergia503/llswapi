﻿using Autofac;
using LittleLimitedSWAPI.DataAccess.EF;
using LittleLimitedSWAPI.DataAccess.Initialize;
using LittleLimitedSWAPI.Infrastructure.Extensions;
using LittleLimitedSWAPI.Infrastructure.IoC;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading;

namespace LittleLimitedSWAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("Policy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyHeader()
                       .AllowAnyMethod();
            }));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSwaggerGen(s =>
            {
                s.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
                {
                    Title = "Little Limited SWAPI",
                    Version = "v1.0.0"
                });
            });

            services.AddTransient<IDataInitializer, DataInitializer>();
            var connectionString = Environment.GetEnvironmentVariable("DB_CONNECTION_STRING");

            services.AddDbContext<LLContext>(options =>
            {
                options.UseSqlServer(connectionString);
            });
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new ContainerModule());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, LLContext llContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var dataInitializer = app.ApplicationServices.GetService<IDataInitializer>();
            RunMissingMigrationsAndSeedDatabaseIfNeeded(dataInitializer, llContext);
            app.UseCors("Policy");

            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(s =>
            {
                s.SwaggerEndpoint("/swagger/v1/swagger.json", "Little Limited SWAPI");
            });
        }

        private void RunMissingMigrationsAndSeedDatabaseIfNeeded(IDataInitializer dataInitializer, LLContext llContext)
        {
            bool shouldWaitForDbCreation = Configuration["Database:ShouldWaitForDBCreation"]
                .ToBool(defaultValue: false);

            if (shouldWaitForDbCreation)
            {
                Console.WriteLine("Waiting 20 seconds for db to be created...");
                Thread.Sleep(20000);
            }

            Console.WriteLine("Migrating database schema with EF Core mechanism.");
            llContext.Database.Migrate();
            Console.WriteLine("Database schema migration completed.");

            bool shouldSeed = Configuration["Database:ShouldSeedDatabase"]
                .ToBool(defaultValue: false);

            if (shouldSeed)
            {
                Console.WriteLine("Seeding database with dummy data...");
                dataInitializer.InitializeDatabase();
                Console.WriteLine("Seeding completed");
            }
        }
    }
}