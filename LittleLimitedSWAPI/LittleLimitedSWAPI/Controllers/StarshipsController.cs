﻿using LittleLimitedSWAPI.Domain;
using LittleLimitedSWAPI.Domain.Paging;
using LittleLimitedSWAPI.Domain.Starships.Queries;
using LittleLimitedSWAPI.Domain.Starships.ReadModels;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LittleLimitedSWAPI.Controllers
{
    public class StarshipsController : ApiBaseController
    {
        public StarshipsController(
            ICommandDispatcher commandDispatcher,
            IQueryDispatcher queryDispatcher)
            : base(commandDispatcher, queryDispatcher)
        {
        }

        [HttpGet("get-starship-by-id")]
        public async Task<IActionResult> GetStarshipById(GetStarshipById query)
        {
            StarshipWithAttributesReadModel starship = await DispatchQueryAsync<GetStarshipById, StarshipWithAttributesReadModel>(query);
            return Json(starship);
        }

        [HttpGet("get-starships")]
        public async Task<IActionResult> GetStarships([FromQuery]GetStarships query)
        {
            PagedResult<StarshipReadModel> starships = await DispatchQueryAsync<GetStarships, PagedResult<StarshipReadModel>>(query);
            return Json(starships);
        }

        [HttpGet("calculate-winner")]
        public async Task<IActionResult> CalculateStarshipWinner([FromQuery]CalculateStarshipWinner query)
        {
            StarshipWinnerReadModel winner = await DispatchQueryAsync<CalculateStarshipWinner, StarshipWinnerReadModel>(query);
            return Json(winner);
        }

        [HttpGet("get-starships-filtered-by-given-attribute")]
        public async Task<IActionResult> GetStarshipsFilteredByGivenAttribute([FromQuery]GetStarshipsFilteredByGivenAttribute query)
        {
            PagedResult<StarshipsFilteredByAttributeReadModel> filteredStarships =
                await DispatchQueryAsync<GetStarshipsFilteredByGivenAttribute, PagedResult<StarshipsFilteredByAttributeReadModel>>(query);

            return Json(filteredStarships);
        }
    }
}
