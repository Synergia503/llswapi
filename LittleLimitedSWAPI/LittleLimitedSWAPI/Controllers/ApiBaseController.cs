﻿using LittleLimitedSWAPI.Domain;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LittleLimitedSWAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class ApiBaseController : Controller
    {
        private readonly ICommandDispatcher _commandDispatcher;

        private readonly IQueryDispatcher _queryDispatcher;

        protected ApiBaseController(ICommandDispatcher commandDispatcher, IQueryDispatcher queryDispatcher)
        {
            _commandDispatcher = commandDispatcher;
            _queryDispatcher = queryDispatcher;
        }

        protected async Task DispatchCommandAsync<TCommand>(TCommand command) where TCommand : ICommand, new()
        {
            await _commandDispatcher.DispatchCommandAsync(command);
        }

        protected async Task<TResult> DispatchQueryAsync<TQuery, TResult>(TQuery query) where TQuery : IQuery where TResult : new()
        {
            return await _queryDispatcher.DispatchQueryAsync<TQuery, TResult>(query);
        }
    }
}