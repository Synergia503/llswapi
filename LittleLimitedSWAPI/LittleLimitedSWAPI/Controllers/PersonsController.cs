﻿using LittleLimitedSWAPI.Domain;
using LittleLimitedSWAPI.Domain.Paging;
using LittleLimitedSWAPI.Domain.People.Queries;
using LittleLimitedSWAPI.Domain.People.ReadModels;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LittleLimitedSWAPI.Controllers
{
    public class PersonsController : ApiBaseController
    {
        public PersonsController(
            ICommandDispatcher commandDispatcher,
            IQueryDispatcher queryDispatcher)
            : base(commandDispatcher, queryDispatcher)
        {
        }

        [HttpGet("get-person-by-id")]
        public async Task<IActionResult> GetPersonById(GetPersonById query)
        {
            PersonWithAttributesReadModel person = await DispatchQueryAsync<GetPersonById, PersonWithAttributesReadModel>(query);
            return Json(person);
        }

        [HttpGet("get-people")]
        // TODO: BJ [FromQuery] attribute is used here due to the swagger. 
        // It doesn't allow to have [FromBody] on GET method. Maybe it can be configured somehow in swagger's settings.
        public async Task<IActionResult> GetPeople([FromQuery]GetPeople query)
        {
            PagedResult<PersonReadModel> people = await DispatchQueryAsync<GetPeople, PagedResult<PersonReadModel>>(query);
            return Json(people);
        }

        [HttpGet("calculate-winner")]
        public async Task<IActionResult> CalculatePersonWinner([FromQuery]CalculatePersonWinner query)
        {
            PersonWinnerReadModel winner = await DispatchQueryAsync<CalculatePersonWinner, PersonWinnerReadModel>(query);
            return Json(winner);
        }

        [HttpGet("get-people-filtered-by-given-attribute")]
        public async Task<IActionResult> GetPeopleFilteredByGivenAttribute([FromQuery]GetPeopleFilteredByGivenAttribute query)
        {
            PagedResult<PeopleFilteredByAttributeReadModel> filteredPeople =
                await DispatchQueryAsync<GetPeopleFilteredByGivenAttribute, PagedResult<PeopleFilteredByAttributeReadModel>>(query);

            return Json(filteredPeople);
        }
    }
}