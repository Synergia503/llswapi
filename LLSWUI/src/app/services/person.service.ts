import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Person, PagedResult } from 'src/app/people/person';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  people: Person[] = [
    { personId: 11, personName: 'Dr Nice' },
    { personId: 12, personName: 'Narco' },
    { personId: 13, personName: 'Bombasto' },
    { personId: 14, personName: 'Celeritas' },
    { personId: 15, personName: 'Magneta' },
    { personId: 16, personName: 'RubberMan' },
    { personId: 17, personName: 'Dynama' },
    { personId: 18, personName: 'Dr IQ' },
    { personId: 19, personName: 'Magma' },
    { personId: 20, personName: 'Tornado' }
  ];
  constructor(private http: HttpClient) { }

  getFromServer(): Observable<PagedResult<Person>> {
    // more on that in read.me
    // return this.http.get<PagedResult<Person>>("http://192.168.99.100:7070/api/persons/get-people?PageNumber=1&PageSize=5");
    return this.http.get<PagedResult<Person>>("http://localhost:5000/api/persons/get-people?PageNumber=1&PageSize=5");
  }
}
