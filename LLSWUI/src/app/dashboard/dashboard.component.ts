import { Component, OnInit } from '@angular/core';
import { Person } from '../people/person';
import { PersonService } from '../services/person.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  people: Person[] = [];

  constructor(private personService: PersonService) { }

  ngOnInit() {
    this.getPeople();
  }


  getPeople(): void {
    this.personService.getFromServer()
      .subscribe(people => this.people = people.items);
  }
}