import { Component, OnInit } from '@angular/core';
import { Person } from './person';
import { PersonService } from '../services/person.service';
import { MessageService } from '../services/message.service';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent implements OnInit {

  people: Person[];
  selectedPerson: Person;

  constructor(private personService: PersonService, private messageService: MessageService) { }

  ngOnInit(): void {

    this.personService.getFromServer().subscribe(people => this.people = people.items);
  }
}