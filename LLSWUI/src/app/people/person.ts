export interface Person {
  personId: number;
  personName: string;
}

export interface PagedResult<T> {
  items: T[];
  totalCount: number;
}